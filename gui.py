import json
import tkinter
from tkinter import filedialog
from typing import Optional

import customtkinter  # type: ignore

from main import run_generator
from src.arguments.arguments import Arguments
from src.gui.ruleset import Ruleset
from src.gui.utils import show_error_dialog


class GraphGenerator(customtkinter.CTk):  # type: ignore
    def __init__(self) -> None:
        super().__init__()
        self.ruleset_path: Optional[str] = None
        self.graph_path: Optional[str] = None
        self.input_path: Optional[str] = None
        self.syslog_path: Optional[str] = None
        self.ipflow_path: Optional[str] = None
        self.attack_goal_path: Optional[str] = None

        self.show_graph = tkinter.BooleanVar()
        self.generate_input = tkinter.BooleanVar()
        self.start_logs = tkinter.BooleanVar()
        self.end_logs = tkinter.BooleanVar()
        self.filter_ns = tkinter.BooleanVar()

        self.title("Kill Chain Attack Graph generator")
        self.geometry("1000x600")

        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=3)
        self.columnconfigure(2, weight=3)
        self.columnconfigure(3, weight=3)
        self.columnconfigure(4, weight=1)
        self.columnconfigure(5, weight=1)
        self.rowconfigure(0, minsize=30)
        self.rowconfigure(10, minsize=30)

        ruleset = customtkinter.CTkLabel(self, text="Ruleset file path:")
        ruleset.grid(row=1, column=1, sticky="e")
        self.selected_ruleset_lab = customtkinter.CTkLabel(self, text="")
        self.selected_ruleset_lab.grid(row=1, column=2)
        ruleset_but = customtkinter.CTkButton(self, text="Select", command=self.select_ruleset)
        ruleset_but.grid(row=1, column=3, sticky="w", padx=5, pady=5)

        graph = customtkinter.CTkLabel(self, text="Output graph path:")
        graph.grid(row=2, column=1, sticky="e")
        self.selected_graph_lab = customtkinter.CTkLabel(self, text="")
        self.selected_graph_lab.grid(row=2, column=2)
        graph_but = customtkinter.CTkButton(self, text="Select", command=self.select_graph)
        graph_but.grid(row=2, column=3, sticky="w", padx=5, pady=5)
        show_graph_checkbox = customtkinter.CTkCheckBox(master=self, variable=self.show_graph, text="Show graph")
        show_graph_checkbox.grid(row=2, column=4, sticky="w")

        input_file = customtkinter.CTkLabel(self, text="Input file path:")
        input_file.grid(row=3, column=1, sticky="e")
        self.selected_input_lab = customtkinter.CTkLabel(self, text="")
        self.selected_input_lab.grid(row=3, column=2)
        input_but = customtkinter.CTkButton(self, text="Select", command=self.select_input)
        input_but.grid(row=3, column=3, sticky="w", padx=5, pady=5)
        generate_input_cb = customtkinter.CTkCheckBox(master=self, text="Generate input", variable=self.generate_input,
                                                      command=self.generate_switched)
        generate_input_cb.grid(row=3, column=4, sticky="w")

        self.syslog_lab = customtkinter.CTkLabel(self, text="Syslog file path:")
        self.syslog_lab.grid(row=4, column=1, sticky="e")
        self.selected_syslog_lab = customtkinter.CTkLabel(self, text="")
        self.selected_syslog_lab.grid(row=4, column=2)
        self.syslog_but = customtkinter.CTkButton(self, text="Select", command=self.select_syslog)
        self.syslog_but.grid(row=4, column=3, sticky="w", padx=5, pady=5)

        self.ipflow_lab = customtkinter.CTkLabel(self, text="Ipflow file path:")
        self.ipflow_lab.grid(row=5, column=1, sticky="e")
        self.selected_ipflow_lab = customtkinter.CTkLabel(self, text="")
        self.selected_ipflow_lab.grid(row=5, column=2)
        self.ipflow_but = customtkinter.CTkButton(self, text="Select", command=self.select_ipflow)
        self.ipflow_but.grid(row=5, column=3, sticky="w", padx=5, pady=5)

        self.attack_goal_lab = customtkinter.CTkLabel(self, text="Attack goals file path:")
        self.attack_goal_lab.grid(row=6, column=1, sticky="e")
        self.selected_attack_goal_lab = customtkinter.CTkLabel(self, text="")
        self.selected_attack_goal_lab.grid(row=6, column=2)
        self.attack_goal_but = customtkinter.CTkButton(self, text="Select", command=self.select_attack_goal)
        self.attack_goal_but.grid(row=6, column=3, sticky="w", padx=5, pady=5)

        validate_command = (self.register(self.validate_number_input))

        self.start_logs_lab = customtkinter.CTkLabel(self, text="Start timespamp:")
        self.start_logs_lab.grid(row=7, column=1, sticky="e")
        self.start_logs_entry = customtkinter.CTkEntry(self, validate='all', validatecommand=(validate_command, '%P'))
        self.start_logs_entry.grid(row=7, column=2, sticky="we", padx=15, pady=5)
        self.start_checkbox = customtkinter.CTkCheckBox(master=self, text="Filter logs by start time",
                                                        variable=self.start_logs)
        self.start_checkbox.grid(row=7, column=3, sticky="w", padx=5, pady=5)

        self.end_logs_lab = customtkinter.CTkLabel(self, text="End timespamp:")
        self.end_logs_lab.grid(row=8, column=1, sticky="e")
        self.end_logs_entry = customtkinter.CTkEntry(self, validate='all', validatecommand=(validate_command, '%P'))
        self.end_logs_entry.grid(row=8, column=2, sticky="we", padx=15, pady=5)
        self.end_checkbox = customtkinter.CTkCheckBox(master=self, text="Filter logs by end time",
                                                      variable=self.end_logs)
        self.end_checkbox.grid(row=8, column=3, sticky="w", padx=5, pady=5)

        self.ns_min_lab = customtkinter.CTkLabel(self, text="Hostname-port minimum:")
        self.ns_min_lab.grid(row=9, column=1, sticky="e")
        self.ns_min_entry = customtkinter.CTkEntry(self, validate='all', validatecommand=(validate_command, '%P'))
        self.ns_min_entry.grid(row=9, column=2, sticky="we", padx=15, pady=5)
        self.ns_min_checkbox = customtkinter.CTkCheckBox(master=self, text="Network service minimum",
                                                         variable=self.filter_ns)
        self.ns_min_checkbox.grid(row=9, column=3, sticky="w", padx=5, pady=5)

        start_button = customtkinter.CTkButton(self, text="Start", command=self.run)
        start_button.grid(row=12, column=4, padx=5, pady=5, sticky="w")

        ruleset_button = customtkinter.CTkButton(self, text="Create ruleset", command=self.ruleset)
        ruleset_button.grid(row=11, column=4, padx=5, pady=5, sticky="w")

        self.generate_switched()

    @staticmethod
    def validate_number_input(string: str) -> bool:
        return str.isdigit(string) or string == ""

    def generate_switched(self) -> None:
        for widget in [self.syslog_lab, self.selected_syslog_lab, self.syslog_but,
                       self.ipflow_lab, self.selected_ipflow_lab, self.ipflow_but,
                       self.attack_goal_lab, self.selected_attack_goal_lab, self.attack_goal_but,
                       self.start_logs_lab, self.start_logs_entry, self.start_checkbox,
                       self.end_logs_lab, self.end_logs_entry, self.end_checkbox,
                       self.ns_min_lab, self.ns_min_entry, self.ns_min_checkbox]:
            if self.generate_input.get():
                widget.grid()
            else:
                widget.grid_remove()

    def select_ruleset(self) -> None:
        filename = Ruleset.open_ruleset()
        if not filename or not Ruleset.validate_ruleset(filename):
            return
        self.selected_ruleset_lab.configure(text=filename)
        self.ruleset_path = filename

    def select_graph(self) -> None:
        filename = filedialog.asksaveasfilename()
        if not filename:
            return
        self.selected_graph_lab.configure(text=filename)
        self.graph_path = filename

    def select_input(self) -> None:
        if self.generate_input.get():
            filename = filedialog.asksaveasfilename()
            if not filename.endswith(".json"):
                filename += ".json"
        else:
            filename = filedialog.askopenfilename()
            if filename and not self.validate_input_file(filename):
                return
        if not filename:
            return
        self.selected_input_lab.configure(text=filename)
        self.input_path = filename

    def select_syslog(self) -> None:
        filename = filedialog.askopenfilename()
        if not filename:
            return
        self.selected_syslog_lab.configure(text=filename)
        self.syslog_path = filename

    def select_ipflow(self) -> None:
        filename = filedialog.askopenfilename()
        if not filename:
            return
        self.selected_ipflow_lab.configure(text=filename)
        self.ipflow_path = filename

    def select_attack_goal(self) -> None:
        filename = filedialog.askopenfilename()
        if not filename:
            return
        self.selected_attack_goal_lab.configure(text=filename)
        self.attack_goal_path = filename

    def create_arguments(self) -> Optional[Arguments]:
        if not self.ruleset_path:
            show_error_dialog("Ruleset file path not selected!")
            return None
        if not self.graph_path:
            show_error_dialog("Output file path not selected!")
            return None
        if not self.input_path:
            show_error_dialog("Input file path not selected!")
            return None

        if not self.generate_input.get():
            return Arguments(self.input_path, self.ruleset_path, self.graph_path, self.show_graph.get())

        if not self.syslog_path:
            show_error_dialog("Syslog file path not selected!")
            return None
        if not self.ipflow_path:
            show_error_dialog("Ipflow file path not selected!")
            return None
        if not self.attack_goal_path:
            show_error_dialog("Attack goal file path not selected!")
            return None

        if self.start_logs.get() and not self.start_logs_entry.get():
            show_error_dialog("Empty start timestamp!")
            return None
        if self.end_logs.get() and not self.end_logs_entry.get():
            show_error_dialog("Empty end timestamp!")
            return None
        if self.filter_ns.get() and not self.ns_min_entry.get():
            show_error_dialog("Empty network service minimum!")
            return None

        start_timestamp = int(self.start_logs_entry.get()) if self.start_logs.get() else None
        end_timestamp = int(self.end_logs_entry.get()) if self.end_logs.get() else None

        if not self.filter_ns.get():
            return Arguments(self.input_path, self.ruleset_path, self.graph_path, self.show_graph.get(),
                             generate_input=True, syslog_fp=self.syslog_path, ipflow_fp=self.ipflow_path,
                             attack_goal_file_path=self.attack_goal_path, start_timestamp=start_timestamp,
                             end_timestamp=end_timestamp)
        return Arguments(self.input_path, self.ruleset_path, self.graph_path, self.show_graph.get(),
                         generate_input=True, syslog_fp=self.syslog_path, ipflow_fp=self.ipflow_path,
                         attack_goal_file_path=self.attack_goal_path, start_timestamp=start_timestamp,
                         end_timestamp=end_timestamp, ns_min=int(self.ns_min_entry.get()))

    def run(self) -> None:
        arguments = self.create_arguments()
        if not arguments:
            return
        run_generator(arguments)

    @staticmethod
    def ruleset() -> None:
        Ruleset()

    @staticmethod
    def validate_input_file(filename: str) -> bool:
        try:
            with open(filename, "r") as file:
                input_file = json.loads(file.read())
        except json.decoder.JSONDecodeError:
            show_error_dialog("Invalid JSON file!")
            return False

        if not isinstance(input_file, dict):
            show_error_dialog("Invalid input file format!")
            return False

        if set(input_file.keys()) != {"attackGoal", "vertices"}:
            show_error_dialog("Invalid input file format! attackGoal and vertices must be the only keys!")
            return False

        return True


if __name__ == '__main__':
    customtkinter.set_appearance_mode("System")
    customtkinter.set_default_color_theme("blue")

    app = GraphGenerator()
    app.mainloop()
