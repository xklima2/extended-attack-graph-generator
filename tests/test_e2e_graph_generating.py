import sys
sys.path.append('../')

from main import _run

from os import path


def test_e2e_graph_1():
    path_config = path.join("e2e_graph_generating", "example_ruleset.json")
    path_input = path.join("e2e_graph_generating", "example_input_1.json")
    show_graph = False
    path_output = "graph_output_1"
    _run(path_config, path_input, show_graph, path_output)

    with open(path.join("output", path_output + ".gv")) as f:
        output_gv = f.read()

    with open(path.join("e2e_graph_generating", "pattern_example_1.gv")) as f:
        pattern_gv = f.read()

    assert pattern_gv == output_gv


def test_e2e_graph_2():
    path_config = path.join("e2e_graph_generating", "example_ruleset.json")
    path_input = path.join("e2e_graph_generating", "example_input_2.json")
    show_graph = False
    path_output = "graph_output_2"
    _run(path_config, path_input, show_graph, path_output)

    with open(path.join("output", path_output + ".gv")) as f:
        output_gv = f.read()

    with open(path.join("e2e_graph_generating", "pattern_example_2.gv")) as f:
        pattern_gv = f.read()

    assert pattern_gv == output_gv
