# import sys
# sys.path.append('../')

from src.input_generating.input_file_generator import generate_input_file
from src.arguments.arguments import Arguments

import json
from os import path


def compare_dicts(dict_1, dict_2):
    assert dict_1["attackGoal"] == dict_2["attackGoal"]

    vertices_1 = dict_1["vertices"]
    vertices_2 = dict_2["vertices"]
    assert vertices_1.keys() == vertices_2.keys()

    for key in vertices_1.keys():
        if sorted(vertices_1[key]) != sorted(vertices_2[key]):
            return False

    return True


def test_e2e_input_file_1():
    path_input = path.join("e2e_input_generating", "generated_input_1.json")
    path_ruleset = path.join("e2e_input_generating", "ruleset.json")
    path_syslog = path.join("e2e_input_generating", "data_syslog_bt6.json")
    path_ipflow = path.join("e2e_input_generating", "data_bt6.json")
    path_attack_goal = path.join("e2e_input_generating", "attack_goal_1.json")

    arguments = Arguments(path_input, path_ruleset, generate_input=True, syslog_fp=path_syslog,
                          ipflow_fp=path_ipflow, attack_goal_file_path=path_attack_goal,
                          start_timestamp=1553081100, end_timestamp=1553083500)

    generate_input_file(arguments)

    with open(path_input) as f:
        dict_1 = json.load(f)
    with open(path.join("e2e_input_generating", "compare_input_1.json")) as f:
        dict_2 = json.load(f)

    assert compare_dicts(dict_1, dict_2)


def test_e2e_input_file_2():
    path_input = path.join("e2e_input_generating", "generated_input_2.json")
    path_ruleset = path.join("e2e_input_generating", "ruleset.json")
    path_syslog = path.join("e2e_input_generating", "data_syslog_bt6.json")
    path_ipflow = path.join("e2e_input_generating", "data_bt6.json")
    path_attack_goal = path.join("e2e_input_generating", "attack_goal_1.json")

    arguments = Arguments(path_input, path_ruleset, generate_input=True, syslog_fp=path_syslog,
                          ipflow_fp=path_ipflow, attack_goal_file_path=path_attack_goal)

    generate_input_file(arguments)

    with open(path_input) as f:
        dict_1 = json.load(f)
    with open(path.join("e2e_input_generating", "compare_input_2.json")) as f:
        dict_2 = json.load(f)

    assert compare_dicts(dict_1, dict_2)


def test_e2e_input_file_3():
    path_input = path.join("e2e_input_generating", "generated_input_3.json")
    path_ruleset = path.join("e2e_input_generating", "ruleset.json")
    path_syslog = path.join("e2e_input_generating", "data_syslog_bt6.json")
    path_ipflow = path.join("e2e_input_generating", "data_bt6.json")
    path_attack_goal = path.join("e2e_input_generating", "attack_goal_1.json")

    arguments = Arguments(path_input, path_ruleset, generate_input=True, syslog_fp=path_syslog,
                          ipflow_fp=path_ipflow, attack_goal_file_path=path_attack_goal, ns_min=10)
    generate_input_file(arguments)

    with open(path_input) as f:
        dict_1 = json.load(f)
    with open(path.join("e2e_input_generating", "compare_input_3.json")) as f:
        dict_2 = json.load(f)

    assert compare_dicts(dict_1, dict_2)
