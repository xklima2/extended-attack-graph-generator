from src.vertices.technique import Technique
from src.vertices.level import Level

from src.enums.phases_enum import Phase

from src.kc_phase_assignment.phase_asignment import assign_kill_chain_phases


def test_example_input_1():
    _result_vertex_fake = Level("Fake result vertex", [])
    external_actor = Level("externalActor", [])

    t1594 = Technique("sentEmail", {"id": "T1594", "name": "Seatch Victim-Owned Websites"}, {})
    t1594.conditions.append(external_actor)
    t1594.result_vertex = _result_vertex_fake

    sent_email_1 = Level("sentEmail", [])
    sent_email_1.previous_techniques.append(t1594)

    t1566_001 = Technique("sentEmail", {"id": "T1566.001", "name": "Spearphising Attachment"}, {})
    t1566_001.conditions.append(sent_email_1)
    t1566_001.result_vertex = _result_vertex_fake

    sent_email_2 = Level("sentEmail", [])
    sent_email_2.previous_techniques.append(t1566_001)

    t1204_002 = Technique("openFile", {"id": "T1204.002", "name": "User Execution - Malicious File"}, {})
    t1204_002.conditions.append(sent_email_2)
    t1204_002.result_vertex = _result_vertex_fake

    open_file = Level("openFile", [])
    open_file.previous_techniques.append(t1204_002)

    t1203 = Technique("system", {"id": "T1203", "name": "Exploitation for Client Execution"}, {})
    t1203.conditions.append(open_file)
    t1203.result_vertex = _result_vertex_fake

    system_1 = Level("system", [])
    system_1.previous_techniques.append(t1203)

    t1068 = Technique("system", {"id": "T1068", "name": "Exploitation for Privilege Escalation"}, {})
    t1068.conditions.append(system_1)
    t1068.result_vertex = _result_vertex_fake

    system_2 = Level("system", [])
    system_2.previous_techniques.append(t1068)

    t1486 = Technique("file", {"id": "T1486", "name": "Data Encrypted for Impact"}, {})
    t1486.conditions.append(system_2)
    t1486.result_vertex = _result_vertex_fake

    t1485 = Technique("file", {"id": "T1485", "name": "Data Destruction"}, {})
    t1485.conditions.append(system_2)
    t1485.result_vertex = _result_vertex_fake

    attack_goal = Level("file", [])
    attack_goal.previous_techniques.append(t1486)
    attack_goal.previous_techniques.append(t1485)

    assign_kill_chain_phases(attack_goal)

    assert len(t1594.kc_phases) == 1 and Phase.RECONNAISSANCE in t1594.kc_phases
    assert len(t1566_001.kc_phases) == 1 and Phase.INITIAL_ACCESS in t1566_001.kc_phases
    assert len(t1204_002.kc_phases) == 1 and Phase.EXECUTION in t1204_002.kc_phases
    assert len(t1203.kc_phases) == 1 and Phase.EXECUTION in t1203.kc_phases
    assert len(t1068.kc_phases) == 1 and Phase.PRIVILEGE_ESCALATION in t1068.kc_phases
    assert len(t1486.kc_phases) == 1 and Phase.IMPACT in t1486.kc_phases
    assert len(t1485.kc_phases) == 1 and Phase.IMPACT in t1485.kc_phases


def test_example_input_2():
    _result_vertex_fake = Level("Fake result vertex", [])
    external_actor = Level("externalActor", [])

    t1595 = Technique("network_connection", {"id": "T1595", "name": "Active Scanning"}, {})
    t1595.conditions.append(external_actor)
    t1595.result_vertex = _result_vertex_fake

    network_connection = Level("networkConnection", [])
    network_connection.previous_techniques.append(t1595)

    t1110 = Technique("account", {"id": "T1110", "name": "Brute Force"}, {})
    t1110.conditions.append(network_connection)
    t1110.result_vertex = _result_vertex_fake

    account = Level("account", [])
    account.previous_techniques.append(t1110)

    t1078 = Technique("application", {"id": "T1078", "name": "Valid Accounts"}, {})
    t1078.conditions.append(account)
    t1078.result_vertex = _result_vertex_fake

    application = Level("application", [])
    application.previous_techniques.append(t1078)

    t1021 = Technique("system", {"id": "T1021", "name": "External Remote Services"}, {})
    t1021.conditions.append(application)
    t1021.result_vertex = _result_vertex_fake

    system = Level("system", [])
    system.previous_techniques.append(t1021)

    t1083 = Technique("file", {"id": "T1083", "name": "File and Directory Discovery"}, {})
    t1083.conditions.append(system)
    t1083.result_vertex = _result_vertex_fake

    file = Level("file", [])
    file.previous_techniques.append(t1083)

    t1005 = Technique("file", {"id": "T1005", "name": "Daa From Local System"}, {})
    t1005.conditions.append(file)
    t1005.conditions.append(system)
    t1005.result_vertex = _result_vertex_fake

    attack_goal = Level("file", [])
    attack_goal.previous_techniques.append(t1005)

    assign_kill_chain_phases(attack_goal)

    assert len(t1595.kc_phases) == 1 and Phase.RECONNAISSANCE in t1595.kc_phases
    assert len(t1110.kc_phases) == 1 and Phase.CREDENTIAL_ACCESS in t1110.kc_phases
    assert len(t1078.kc_phases) == 1 and Phase.PRIVILEGE_ESCALATION in t1078.kc_phases
    assert len(t1021.kc_phases) == 1 and Phase.LATERAL_MOVEMENT in t1021.kc_phases
    assert len(t1083.kc_phases) == 0
    assert len(t1005.kc_phases) == 1 and Phase.COLLECTION in t1005.kc_phases


def test_2_phases_keep_1():
    _result_vertex_fake = Level("Fake result vertex", [])
    level_1 = Level("", [])

    t1594 = Technique("", {"id": "T1594", "name": ""}, {})  # Reconnaissance
    t1594.conditions.append(level_1)
    t1594.result_vertex = _result_vertex_fake

    level_2 = Level("", [])
    level_2.previous_techniques.append(t1594)

    t1078 = Technique("", {"id": "T1078", "name": ""}, {})  # InitialAccess/PrivilegeEscalation
    t1078.conditions.append(level_2)
    t1078.result_vertex = _result_vertex_fake

    level_3 = Level("", [])
    level_3.previous_techniques.append(t1078)

    t1190 = Technique("", {"id": "T1190", "name": ""}, {})  # InitialAccess
    t1190.conditions.append(level_3)
    t1190.result_vertex = _result_vertex_fake

    level_4 = Level("", [])
    level_4.previous_techniques.append(t1190)
    level_4.previous_techniques.append(t1078)

    t1498 = Technique("", {"id": "T1498", "name": ""}, {})  # Impact
    t1498.conditions.append(level_4)
    t1498.result_vertex = _result_vertex_fake

    attack_goal = Level("", [])
    attack_goal.previous_techniques.append(t1498)

    assign_kill_chain_phases(attack_goal)

    assert len(t1594.kc_phases) == 1 and Phase.RECONNAISSANCE in t1594.kc_phases
    assert len(t1078.kc_phases) == 1 and Phase.INITIAL_ACCESS in t1078.kc_phases
    assert len(t1190.kc_phases) == 1 and Phase.INITIAL_ACCESS in t1190.kc_phases
    assert len(t1498.kc_phases) == 1 and Phase.IMPACT in t1498.kc_phases


def test_2_phases_keep_1_reverse():
    _result_vertex_fake = Level("Fake result vertex", [])
    level_1 = Level("", [])

    t1594 = Technique("", {"id": "T1594", "name": ""}, {})  # Reconnaissance
    t1594.conditions.append(level_1)
    t1594.result_vertex = _result_vertex_fake

    level_2 = Level("", [])
    level_2.previous_techniques.append(t1594)

    t1078 = Technique("", {"id": "T1078", "name": ""}, {})  # InitialAccess/PrivilegeEscalation
    t1078.conditions.append(level_2)
    t1078.result_vertex = _result_vertex_fake

    level_3 = Level("", [])
    level_3.previous_techniques.append(t1078)

    t1190 = Technique("", {"id": "T1190", "name": ""}, {})  # InitialAccess
    t1190.conditions.append(level_3)
    t1190.result_vertex = _result_vertex_fake

    level_4 = Level("", [])
    level_4.previous_techniques.append(t1078)
    level_4.previous_techniques.append(t1190)

    t1498 = Technique("", {"id": "T1498", "name": ""}, {})  # Impact
    t1498.conditions.append(level_4)
    t1498.result_vertex = _result_vertex_fake

    attack_goal = Level("", [])
    attack_goal.previous_techniques.append(t1498)

    assign_kill_chain_phases(attack_goal)

    assert len(t1594.kc_phases) == 1 and Phase.RECONNAISSANCE in t1594.kc_phases
    assert len(t1078.kc_phases) == 1 and Phase.INITIAL_ACCESS in t1078.kc_phases
    assert len(t1190.kc_phases) == 1 and Phase.INITIAL_ACCESS in t1190.kc_phases
    assert len(t1498.kc_phases) == 1 and Phase.IMPACT in t1498.kc_phases


def test_2_phases_keep_2():
    _result_vertex_fake = Level("Fake result vertex", [])
    level_1 = Level("", [])

    t1594 = Technique("", {"id": "T1594", "name": ""}, {})  # Reconnaissance
    t1594.conditions.append(level_1)
    t1594.result_vertex = _result_vertex_fake

    level_2 = Level("", [])
    level_2.previous_techniques.append(t1594)

    t1078 = Technique("", {"id": "T1078", "name": ""}, {})  # InitialAccess/PrivilegeEscalation
    t1078.conditions.append(level_2)
    t1078.result_vertex = _result_vertex_fake

    level_3 = Level("", [])
    level_3.previous_techniques.append(t1078)

    t1049 = Technique("", {"id": "T1049", "name": ""}, {})  # Discovery
    t1049.conditions.append(level_3)
    t1049.result_vertex = _result_vertex_fake

    level_4 = Level("", [])
    level_4.previous_techniques.append(t1049)
    level_4.previous_techniques.append(t1078)

    t1498 = Technique("", {"id": "T1498", "name": ""}, {})  # Impact
    t1498.conditions.append(level_4)
    t1498.result_vertex = _result_vertex_fake

    attack_goal = Level("", [])
    attack_goal.previous_techniques.append(t1498)

    assign_kill_chain_phases(attack_goal)

    assert len(t1594.kc_phases) == 1 and Phase.RECONNAISSANCE in t1594.kc_phases
    assert (len(t1078.kc_phases) == 2 and Phase.INITIAL_ACCESS in t1078.kc_phases
            and Phase.PRIVILEGE_ESCALATION in t1078.kc_phases)
    assert len(t1049.kc_phases) == 1 and Phase.DISCOVERY in t1049.kc_phases
    assert len(t1498.kc_phases) == 1 and Phase.IMPACT in t1498.kc_phases


def test_2_phases_keep_2_reverse():
    _result_vertex_fake = Level("Fake result vertex", [])
    level_1 = Level("", [])

    t1594 = Technique("", {"id": "T1594", "name": ""}, {})  # Reconnaissance
    t1594.conditions.append(level_1)
    t1594.result_vertex = _result_vertex_fake

    level_2 = Level("", [])
    level_2.previous_techniques.append(t1594)

    t1078 = Technique("", {"id": "T1078", "name": ""}, {})  # InitialAccess/PrivilegeEscalation
    t1078.conditions.append(level_2)
    t1078.result_vertex = _result_vertex_fake

    level_3 = Level("", [])
    level_3.previous_techniques.append(t1078)

    t1049 = Technique("", {"id": "T1049", "name": ""}, {})  # Discovery
    t1049.conditions.append(level_3)
    t1049.result_vertex = _result_vertex_fake

    level_4 = Level("", [])
    level_4.previous_techniques.append(t1078)
    level_4.previous_techniques.append(t1049)

    t1498 = Technique("", {"id": "T1498", "name": ""}, {})  # Impact
    t1498.conditions.append(level_4)
    t1498.result_vertex = _result_vertex_fake

    attack_goal = Level("", [])
    attack_goal.previous_techniques.append(t1498)

    assign_kill_chain_phases(attack_goal)

    assert len(t1594.kc_phases) == 1 and Phase.RECONNAISSANCE in t1594.kc_phases
    assert (len(t1078.kc_phases) == 2 and Phase.INITIAL_ACCESS in t1078.kc_phases
            and Phase.PRIVILEGE_ESCALATION in t1078.kc_phases)
    assert len(t1049.kc_phases) == 1 and Phase.DISCOVERY in t1049.kc_phases
    assert len(t1498.kc_phases) == 1 and Phase.IMPACT in t1498.kc_phases


if __name__ == '__main__':
    test_example_input_1()
    test_example_input_2()
    test_2_phases_keep_1()
    test_2_phases_keep_1_reverse()
    test_2_phases_keep_2()
    test_2_phases_keep_2_reverse()
