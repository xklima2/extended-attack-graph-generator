from test_e2e_graph_generating import *
from test_phase_asignment import *
from test_e2e_input_generating import *


def main():
    test_e2e_graph_1()
    test_e2e_graph_2()
    test_example_input_1()
    test_example_input_2()
    test_2_phases_keep_1()
    test_2_phases_keep_1_reverse()
    test_2_phases_keep_2()
    test_2_phases_keep_2_reverse()
    test_e2e_input_file_1()
    test_e2e_input_file_2()
    test_e2e_input_file_3()


if __name__ == '__main__':
    main()
    print("All tests passed")
