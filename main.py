import os.path
from datetime import datetime

import argparse

from src.graph_generating.diagram import Diagram
from src.graph_generating.logic import Logic
from src.parsers.input_parser import InputParser
from src.arguments.arguments import Arguments
from src.input_generating.input_file_generator import generate_input_file


def main() -> None:
    parsed_args = _create_parser()
    run_generator(parsed_args)


def run_generator(arguments: Arguments) -> None:
    if arguments.generate_input:
        if (arguments.syslog_file_path is None or arguments.ipflow_file_path is None or
                arguments.attack_goal_file_path is None):
            return
        input_file_paths = generate_input_file(arguments)
        if len(input_file_paths) == 1:
            _run(arguments.ruleset_path, input_file_paths[0], arguments.show_graph, arguments.output_path)
            return

        for i, input_file_path in enumerate(input_file_paths):
            file_name, extension = os.path.splitext(arguments.output_path)
            output_path = f"{file_name}_{i}{extension}"
            _run(arguments.ruleset_path, input_file_path, arguments.show_graph, output_path)
        return

    _run(arguments.ruleset_path, arguments.input_file_path, arguments.show_graph, arguments.output_path)


def _create_parser() -> Arguments:
    """Parses the commandline input
    
        Returns
        -------
        argparse.Namespace
            parsed arguments
    """
    parser = argparse.ArgumentParser(
        prog='Attack graph generator',
        description='Created attack graph based on the given configuration and input file')

    parser.add_argument('--ruleset_file', help='Path to the ruleset file')
    parser.add_argument('--input_file', help='Path for the output files')
    parser.add_argument('-o', '--path_output', help='Name of the output file', required=False, default="output")
    parser.add_argument('-s', '--show_graph', action='store_true',  required=False,
                        help='Show diagram after the algorithm run')
    parser.add_argument('-g', '--generate_input', action='store_true',
                        help='Generate input file from Syslog and IPFIX output files')
    parser.add_argument('--syslog_file',
                        help='Path to file with Syslog logs (required if -g/--generate_input is provided)')
    parser.add_argument('--ipfix_file',
                        help='Path to file with IPFIX flows (required if -g/--generate_input is provided)')
    parser.add_argument('--attack_goal_file',
                        help='Path to file with attack goal(s) (required if -g/--generate_input is provided)')
    parser.add_argument('--start', default=None, help='Starting point to filter the logs')
    parser.add_argument('--end', default=None, help='Ending point to filter the logs')
    parser.add_argument('--ns_min', default=1, help='Minimum of hostname - port pair occurrences to generate '
                                                    'networkSerice attribute')

    args = parser.parse_args()

    if args.generate_input and not args.syslog_file:
        parser.error('--syslog_file is required when using -g/--generate_input')
    if args.generate_input and not args.ipfix_file:
        parser.error('--ipfix_file is required when using -g/--generate_input')

    if args.start:
        try:
            args.start = int(args.start)
        except ValueError:
            parser.error('Invalid start timestamp')

    if args.end:
        try:
            args.end = int(args.end)
        except ValueError:
            parser.error('Invalid end timestamp')

    try:
        args.ns_min = int(args.ns_min)
    except ValueError:
        parser.error('Invalid networkService hostname - port occurrences minimum')

    return Arguments(args.input_file, args.ruleset_file, args.path_output, args.show_graph, args.generate_input,
                     args.syslog_file, args.ipfix_file, args.attack_goal_file, args.start, args.end, args.ns_min)


def _run(path_config: str, path_input: str, show_graph: bool = False, path_output: str = "output") -> None:
    """Prepares data, runs the discovery algorithm and creates the diagram

        Parameters
        ----------
        path_config : str
            Path of the configuration file
        path_input : str
            Path of the input file
        show_graph: bool
            Show diagram after the algorithm run if True
        path_output: str
            Name of the output file

    """
    current_time = datetime.now().strftime("%H:%M:%S")
    print("Generating started at: ", current_time)

    parser = InputParser()

    predicates, ruleset = parser.parse_configuration_file(path_config)
    attack_goal, attack_factors = parser.parse_input_file(path_input, predicates)

    logic = Logic(predicates, ruleset, attack_goal, attack_factors)

    g = logic.create_graph()

    Diagram(g, path_output, show_graph, attack_factors).create_diagram()

    current_time = datetime.now().strftime("%H:%M:%S")
    print("Generating ended at: ", current_time)


if __name__ == "__main__":
    main()
