# Extended Attack Graph Generator

The Extended Attack Graph Generator is a Python tool designed to generate kill chain attack graphs from collected Syslog logs and
IPFIX entries. This tool extends the original [Attack Graph Generator](https://is.muni.cz/auth/th/twplc/) developed by
Oldřich Machala at Masaryk University.
It offers both command line and GUI options for ease of use.

## Installation

1. Install Python 3.10.0
2. Make sure you have the [tkinter library](https://docs.python.org/3/library/tkinter.html) installed
3. Clone the repository and install dependencies:

```bash
git clone git@gitlab.fi.muni.cz:xklima2/extended-attack-graph-generator.git
cd extended-attack-graph-generator
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Usage

The Extended Attack Graph Generator can be used via the command line or through the provided GUI.

### Command Line

To run the tool from the command line, use the following arguments:

| Argument                 | Description                                                                      | Required    | Default Value |
|--------------------------|----------------------------------------------------------------------------------|-------------|---------------|
| `--ruleset_file`         | Path to the ruleset file                                                         | Yes         | -             |
| `--input_file`           | Path to the input file                                                           | Yes         | -             |
| `-o`, `--path_output`    | Path for the output files                                                        | No          | "output"      |
| `-s`, `--show_graph`     | Show diagram after the algorithm run                                             | No          | False         |
| `-g`, `--generate_input` | Generate input file from Syslog and IPFlow output files                          | No          | False         |
| `--syslog_file`          | Path to file with Syslog logs (required if -g/--generate_input is provided)      | Conditional | -             |
| `--ipflix_file`          | Path to file with IPFlow flows (required if -g/--generate_input is provided)     | Conditional | -             |
| `--attack_goal_file`     | Path to file with attack goal(s) (required if -g/--generate_input is provided)   | Conditional | -             |
| `--start`                | Starting point to filter the logs                                                | No          | None          |
| `--end`                  | Ending point to filter the logs                                                  | No          | None          |
| `--ns_min`               | Minimum of hostname - port pair occurrences to generate networkService attribute | No          | 1             |

#### Generate graphs from existing input files

```bash
python3.10 -m main --ruleset_file examples/ruleset.json --input_file examples/input_file_1.json -s
```

```bash
python3.10 -m main --ruleset_file examples/ruleset.json --input_file examples/input_file_2.json -s
```

#### Generate graphs and input files from Syslog and IPFIX files

```bash
python3.10 -m main --ruleset_file examples/ruleset.json --input_file generated_input -s -g --syslog_file examples/syslog.json --ipfix_file examples/ipfix.json --attack_goal_file examples/attack_goals.json --ns_min 10
```

### GUI

To run the tool from the GUI, use the following command:

```bash
python3.10 -m gui
```

## Project Structure

```
extended-attack-graph-generator/  
├── main.py                    # Main script to generate attack graphs through command line  
├── gui.py                     # Main script to generate attack graphs through GUI                    
├── src/                       
│   ├── arguments/             # Module for passing arguments developed by Matěj Klima 
│   ├── enums/                 # Module with enums developed by Oldřich Machala and Matěj Klima
│   ├── graph_generating/      # Module for generating attack graphs developed by Oldřich Machala
│   ├── gui/                   # Module with GUI components developed by Matěj Klima
│   ├── input_generating/      # Module for generating input files developed by Matěj Klima
│   │   └── vulnerable_asset/  # Module for generating vulnerable assets developed by Lukáš Sadlek and Martin Laštovička et al.
│   ├── kc_phase_assignment/   # Module for assigning phases to attack steps developed by Matěj Klima
│   ├── parsers/               # Module for parsing input files developed by Oldřich Machala
│   └── vertices/              # Module with vertices developed by Oldřich Machala
├── tests/                     # Directory with tests developed by Matěj Klima  
└── examples/                      
    ├── attack_goals.json      # Example file with two attack goals
    ├── input_file_1.json      # Example input file resulting in graph not violating the partial oredinr of kc phases  
    ├── input_file_2.json      # Example input file resulting in graph violating the partial oredinr of kc phases 
    ├── ipfix.json             # First 5000 IPFIX entries from team 6 from the cyber defense exercise 
    ├── ruleset.json           # Example ruleset file
    └── syslog.json            # First 5000 Syslog logs from team 6 from the cyber defense exercise
```

## References

- [Attack Graph Generator](https://is.muni.cz/th/twplc/) by Oldřich Machala
- [Cyber Defense Exercise](https://www.sciencedirect.com/science/article/pii/S2352340920306788?via%3Dihub) at Masaryk University
- [Vulnerability Exploit Effect Categorization](https://is.muni.cz/th/yyck4/) by Lukáš Sadlek
- [Software for enumerating vulnerabilities in a computer network](https://www.muni.cz/en/research/publications/1724696) by Martin Laštovička et al.

## License

This project is licensed under the MIT License.

