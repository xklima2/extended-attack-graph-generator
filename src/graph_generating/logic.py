from copy import deepcopy
from src.enums.colors_enum import Color
from src.enums.shapes_enum import Shape
from src.graph_generating.helper import Helper

from src.vertices.technique import Technique
from src.vertices.vertex import Vertex

from src.kc_phase_assignment.phase_asignment import assign_kill_chain_phases


class Logic:
    """Holds the information needed to generate an attack graph and contains functions for its generation
    """

    def __init__(self, predicates: dict[str, Vertex], ruleset: dict[str, list[Technique]], attack_goal: Vertex,
                 attack_factors: dict[str, list[Vertex]]) -> None:
        """Generates the class

        Parameters
        ----------
        predicates : dict[str, Vertex]
            contains all vertices which could be found in the attack graph
        ruleset : dict[str, list[Technique]]
            contains all forms of techniques which could be found in the attack graph
        attack_goal : Vertex
            starting point of the search
        attack_factors : dict[str, list[Vertex]]
            end points of the search
        """
        self.predicates = deepcopy(predicates)
        self.ruleset = deepcopy(ruleset)
        self.attack_goal = deepcopy(attack_goal)
        self.attack_factors = deepcopy(attack_factors)

    def _check_possible_attack_factors(self, current_vertex: Vertex) -> list[Vertex]:
        """Check if the given vector is one or more of the vertices we are looking for

        Parameters
        ----------
        current_vertex : Vertex
            vertex to be checked

        Returns
        -------
        list[Vertex]
            list of vertices that are the end goals of the search
            empty if the search shall continue
        """

        valid_attack_factors: list[Vertex] = []
        for attack_factor in self.attack_factors[current_vertex.name]:
            if Helper.eq_with_variables(current_vertex, attack_factor, current_vertex.next_technique):
                valid_attack_factors.append(attack_factor)

        return valid_attack_factors

    @staticmethod
    def _merge_same_techniques(list_of_techniques: list[Technique]) -> list[Technique]:
        """Return given list of techniques without redundancy

        Parameters
        ----------
        list_of_techniques : list[Technique]
            list to be checked for same techniques

        Returns
        -------
        list[Technique]
            list of techniques without redundancy
        """
        same_techniques: list[Technique] = []
        for technique in list_of_techniques:
            not_present = True
            for same_technique in same_techniques:
                if technique == same_technique:
                    not_present = False
                    break
            if not_present:
                same_techniques.append(technique)
        return same_techniques

    def _run_dfs_on_technique(self, copied_technique: Technique, discovered_vertices: list[Vertex]) -> list[Technique]:
        """Goes through copied_technique condition vertices.
        If there are multiple vertices for a technique, split it to fit the vertices.
        First loop indexes condition vertices and defines which one is currently searched
        in the current list of techniques.
        Second loop indexes currently searched technique.
        It indexes backwards so if the technique is not correct, it can be safely removed and new techniques can be
        safely placed at the end and be searched in the next loop.

        Parameters
        ----------
        copied_technique : Technique
            technique on which is algorithm performed
        discovered_vertices : list[Vertex]
            list of vertices already in the tree

        Returns
        -------
        list[Technique]
            list of techniques that are correct, empty if there is no valid path
        """

        techniques: list[Technique] = [copied_technique]
        new_techniques: list[Technique] = []
        for i in range(len(copied_technique.conditions)):
            for k in range(len(techniques) - 1, -1, -1):

                valid_attack_factors: list[Vertex] = self._check_possible_attack_factors(
                    techniques[k].conditions[i])

                # if the current vector is the one from input file, do not continue DFS on it
                # if there are multiple of them, split technique for every one of them
                if valid_attack_factors:
                    creating_techniques = [techniques.pop(k)]
                    for x in range(len(valid_attack_factors) - 1):
                        creating_techniques.append(
                            deepcopy(creating_techniques[0]))

                    for j in range(len(valid_attack_factors)):
                        creating_techniques[j].set_values(
                            valid_attack_factors[j], creating_techniques[j].conditions[i])
                        creating_techniques[j].conditions[i] = valid_attack_factors[j]
                    new_techniques += creating_techniques

                # run DFS on one of the condition vertex in technique, if the search is not
                elif not self._dfs_graph(techniques[k].conditions[i], discovered_vertices):
                    techniques.pop(k)

                # merges techniques together
                elif techniques[k].conditions[i].previous_techniques:
                    same_techniques: list[Technique] = self._merge_same_techniques(
                        techniques[k].conditions[i].previous_techniques)

                    techniques[k].conditions[i].previous_techniques = []
                    creating_techniques = [techniques.pop(k)]
                    for x in range(len(same_techniques) - 1):
                        creating_techniques.append(
                            deepcopy(creating_techniques[0]))

                    for j in range(len(creating_techniques)):
                        value = same_techniques.pop()
                        creating_techniques[j].conditions[i].previous_techniques.append(
                            value)
                        creating_techniques[j].conditions[i].next_technique = creating_techniques[j]

                        creating_techniques[j].set_values_from_variable(
                            value.result_vertex, creating_techniques[j].conditions[i])

                    new_techniques += creating_techniques
            techniques += new_techniques
            new_techniques = []

        return techniques

    def _dfs_graph(self, current_vertex: Vertex, discovered_vertices: list[Vertex]) -> bool:
        """Checks for cycles and if the vertex was discovered already.
        In case of cycle end the search as this is not correct.
        If discovered, connect it with connect it to the tree.
        Finds all the possible techniques which could be created with this vector.


        Parameters
        ----------
        current_vertex : Vertex
            vertex that is being checked
        discovered_vertices : list[Vertex]
            list of vertices already in the tree

        Returns
        -------
        bool
            returns true if the search was successful
        """

        if current_vertex.color != Color.WHITE:
            return True

        for vertex in discovered_vertices:
            if Helper.eq_both_with_variables(vertex, current_vertex, vertex.next_technique,
                                             current_vertex.next_technique):
                assert vertex.color != Color.WHITE
                if vertex.color == Color.GRAY:
                    return False  # is cycle
                # if has technique (should have), set the found vertex to that technique
                if current_vertex.next_technique is not None:
                    index = current_vertex.next_technique.conditions.index(
                        current_vertex)
                    current_vertex.next_technique.conditions[index] = vertex
                return True

        current_vertex.color = Color.GRAY
        discovered_vertices.append(current_vertex)

        is_empty = True

        for technique in self.ruleset[current_vertex.name]:
            if Helper.eq_with_variables(current_vertex, technique.result_vertex, current_vertex.next_technique):
                copied_technique = deepcopy(technique)
                copied_technique.set_values_from_variable(
                    current_vertex, copied_technique.result_vertex)

                current_vertex.previous_techniques += self._run_dfs_on_technique(
                    copied_technique, discovered_vertices)
                is_empty = False

        if len(current_vertex.previous_techniques) == 0 or is_empty:
            discovered_vertices.remove(current_vertex)
            return False

        current_vertex.color = Color.BLACK
        return True

    def create_graph(self) -> Vertex:
        """Runs DFS algorithm on an attack goal

        Returns
        -------
        Vertex
            root of a DFS tree
        """

        discovered_vertices: list[Vertex] = []

        self._dfs_graph(self.attack_goal, discovered_vertices)

        # to merge the remaining techniques in attack_goal
        self.attack_goal.previous_techniques = self._merge_same_techniques(
            self.attack_goal.previous_techniques)

        assign_kill_chain_phases(self.attack_goal)

        self.attack_goal.shape = Shape.ATTACK_GOAL
        self.attack_goal.style = "solid"
        return self.attack_goal
