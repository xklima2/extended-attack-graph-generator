import graphviz  # type: ignore
from src.graph_generating.helper import Helper

from src.vertices.technique import Technique
from src.vertices.vertex import Vertex

from os import path


class Diagram:
    def __init__(self, starting_vertex: Vertex, path_output: str, show_graph: bool,
                 attack_factors: dict[str, list[Vertex]]) -> None:
        """Created Diagram class
            Parameters
            ----------
            starting_vertex: Vertex
                vertex where the diagram generation starts
            path_output: str
                path to the output file
            show_graph: bool
                display the diagram after it is generated
            attack_factors: dict[str, list[Vertex]]
                vertices where the diagram ends
        """
        self.starting_vertex = starting_vertex
        self.path_output = path_output
        self.show_graph = show_graph
        self.attack_factors = attack_factors
        self.discovered_vertices: list[Vertex] = []

    @staticmethod
    def _edge_style(vertex: Vertex, technique: Technique) -> str:
        if vertex.style == "solid" and technique.style == "solid":
            return "solid"
        return "dashed"

    def _add_vertex_to_graph(self, graph: graphviz.Digraph, vertex: Vertex) -> None:
        """Checks if a same vertex is in diagram and if so, connects the given technique with it,
           otherwise connects it with the given vertex

            Parameters
            ----------
            graph: graphviz.Digraph
                diagram that is being created
            vertex: Vertex
                vertex to be added in the diagram
        """
        self.discovered_vertices.append(vertex)

        graph.node(str(vertex), vertex.name + ": (" + self._get_text(vertex) + ")",
                   shape=vertex.shape, style=vertex.style)

        for technique in vertex.previous_techniques:
            graph.edge(str(technique), str(vertex), style=self._edge_style(vertex, technique))
            self._add_technique_to_graph(graph, technique)

    def _add_technique_to_graph(self, graph: graphviz.Digraph, technique: Technique) -> None:
        """Adds technique to diagram and all the vertices connected to it

            Parameters
            ----------
            graph: graphviz.Digraph
                diagram that is being created
            technique: Technique
                technique to be added in the diagram

        """
        graph.node(str(technique), technique.id + ": " +
                   technique.technique_name + "\n" + technique.print_kill_chain_phases(),
                   shape=technique.shape, style=technique.style)
        for vertex in technique.conditions:
            if not self._check_if_was_found(graph, vertex, technique):
                self._add_vertex_to_graph(graph, vertex)

    def _check_if_was_found(self, graph: graphviz.Digraph, vertex: Vertex, technique: Technique) -> bool:
        """Checks if a same vertex is in diagram and if so, connects the given technique with it,
           otherwise connects it with the given vertex

            Parameters
            ----------
            graph: graphviz.Digraph
                diagram that is being created
            vertex: Vertex
                vertex to be checked
            technique: Technique
                technique to which the vertex will be connected

            Returns
            -------
            bool
                returns True if the vertex is already in the diagram
        """
        for discovered_vertex in self.discovered_vertices:
            if vertex == discovered_vertex:
                graph.edge(str(discovered_vertex), str(technique), style=self._edge_style(vertex, technique))
                return True
        graph.edge(str(vertex), str(technique), style=self._edge_style(vertex, technique))
        return False

    def create_diagram(self) -> None:
        """Creates the diagram
        """

        graph = graphviz.Digraph(comment='Attack graph', strict=True)

        self._add_vertex_to_graph(graph, self.starting_vertex)

        graph.render(path.join("output", self.path_output + ".gv"), view=False, format="pdf")
        graph.render(path.join("output", self.path_output + ".gv"), view=self.show_graph, format="svg")

    @staticmethod
    def _get_text(vertex: Vertex) -> str:
        """Creates text for the vertex to be displayed in the diagram

            Parameters
            ----------
            vertex: Vertex
                vertex for which the text should be generated

            Returns
            -------
            str
                generated text from the Vertex
        """
        text: str = ""
        for atr in vertex.attributes:
            if text != "":
                text += ", "
            if Helper.is_variable(atr[1]):
                if vertex.next_technique is None:
                    raise Exception(
                        "Technique with variables not found when creating a diagram")
                text += vertex.next_technique.variables[atr[1]]
            else:
                text += atr[1]
        return text
