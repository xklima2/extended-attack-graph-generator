from typing import Optional
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from src.vertices.vertex import Vertex
    from src.vertices.technique import Technique


class Helper:
    @staticmethod
    def is_var_or_empty(element: str) -> bool:
        """Check if the string is a variable (first letter is uppercase) or is an empty symbol (_)

        Parameters
        ----------
        element : str
            string to be checked

        Returns
        -------
        bool
            True if the string is variable or is empty
        """
        return Helper.is_empty(element) or Helper.is_variable(element)

    @staticmethod
    def is_empty(element: str) -> bool:
        """Check if the string is an empty symbol (_)

        Parameters
        ----------
        element : str
            string to be checked

        Returns
        -------
        bool
            True if the string is empty
        """
        return element == "_"

    @staticmethod
    def is_variable(element: str) -> bool:
        """Check if the string is a variable (first letter is uppercase)

        Parameters
        ----------
        element : str
            string to be checked

        Returns
        -------
        bool
            True if the string is variable
        """
        return len(str(element)) > 0 and str(element)[0].isupper()

    @staticmethod
    def eq_with_variables(v1: 'Vertex', v2: 'Vertex', technique: Optional['Technique']) -> bool:
        """Check if v2 belongs to v1

        Parameters
        ----------
        v1 : Vertex
            vertex in graph
        v2 : Vertex
            vertex which is checked if it could be in the position of v1
        technique : Optional[Technique]
            technique are optional for vertex because when they are created they are set to None

        Returns
        -------
        bool
            True if v2 belongs to v1

        Raises
        ------
        Exception
            technique of vertex was not set
        """
        if v1.name != v2.name:
            return False
        for i in range(len(v1.attributes)):
            if Helper.is_var_or_empty(v2.attributes[i][1]) or Helper.is_empty(v1.attributes[i][1]):
                continue
            if Helper.is_variable(v1.attributes[i][1]):
                if technique is None:
                    raise Exception("technique is None")
                if technique.variables[v1.attributes[i][1]] == "":
                    continue
                if technique.variables[v1.attributes[i][1]] != v2.attributes[i][1]:
                    return False

            elif v1.attributes[i][1] != v2.attributes[i][1]:
                return False

        return True

    @staticmethod
    def eq_both_with_variables(v1: 'Vertex', v2: 'Vertex', technique1: Optional['Technique'],
                               technique2: Optional['Technique']) -> bool:
        """If v2 is equal to v1 even with variables (first letter is uppercase) and an empty symbols (_)

        Parameters
        ----------
        v1 : Vertex
            vertex in graph
        v2 : Vertex
            vertex in graph
        technique1 : Optional[Technique]
            technique of v1
        technique2 : Optional[Technique]
            technique of v2

        Returns
        -------
        bool
            True if the vertices are equal
        """
        if v1.name != v2.name:
            return False
        for i in range(len(v1.attributes)):
            if not Helper.eq_of_one_thing(v1.attributes[i][1], v2.attributes[i][1], technique1, technique2):
                return False
        return True

    @staticmethod
    def eq_of_one_thing(v1: str, v2: str, technique1: Optional['Technique'], technique2: Optional['Technique']) -> bool:
        """If attributes of vertices are same even with variables (first letter is uppercase) and an empty symbols (_)

        Parameters
        ----------
        v1 : str
            attribute of first vertex
        v2 : str
            attribute of second vertex
        technique1 : Optional[Technique]
            technique of first vertex
        technique2 : Optional[Technique]
            technique of second vertex

        Returns
        -------
        bool
            True if the attributes are equal
        """
        if Helper.is_empty(v1) or Helper.is_empty(v2):
            return True

        if Helper.is_variable(v1):
            if technique1 is None:
                v1 = ""
            else:
                v1 = technique1.variables[v1]

        if Helper.is_variable(v2):
            if technique2 is None:
                v2 = ""
            else:
                v2 = technique2.variables[v2]

        return v1 == v2
