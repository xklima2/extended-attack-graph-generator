from typing import Any, Type, Optional, Callable

import customtkinter  # type: ignore

from src.gui.utils import show_error_dialog
from src.gui.vertices.vertex import Vertex


class AddVertexWindow(customtkinter.CTkToplevel):  # type: ignore
    def __init__(self, vertex: Type[Vertex], parent: Any, check_delete_fun: Callable[[str], Optional[str]],
                 vertice_names: set[str]):
        super().__init__()
        self.vertex = vertex
        self.parent = parent
        self.check_delete_fun = check_delete_fun
        self.vertice_names = vertice_names

        self.params_count = 0
        self.params_entries: list[customtkinter.CTkEntry] = []

        self.title(f"Add new {self.vertex.type_str}")
        self.geometry("600x400")

        self.columnconfigure(0, minsize=30)
        self.columnconfigure(1, weight=3)
        self.columnconfigure(2, weight=3)
        self.columnconfigure(3, minsize=30)
        self.rowconfigure(0, minsize=30)

        name = customtkinter.CTkLabel(self, text="Name:")
        name.grid(row=1, column=1, sticky="e", padx=5, pady=5)
        self.entry_name = customtkinter.CTkEntry(self, placeholder_text=f"Enter {self.vertex.type_str} name")
        self.entry_name.grid(row=1, column=2, sticky="w", padx=5, pady=5)

        self.add_param_button = customtkinter.CTkButton(self, text="Add parameter", command=self.add_param)
        self.create_button = customtkinter.CTkButton(self, text=f"Create new {self.vertex.type_str}",
                                                     command=self.create)
        self.add_param()

    def add_param(self) -> None:
        self.params_count += 1
        param = customtkinter.CTkLabel(self, text="Parameter:")
        param.grid(row=self.params_count + 1, column=1, sticky="e", padx=5, pady=5)
        entry_param = customtkinter.CTkEntry(self, placeholder_text="Enter parameter name")
        entry_param.grid(row=self.params_count + 1, column=2, sticky="w", padx=5, pady=5)
        self.params_entries.append(entry_param)
        self.add_param_button.grid(row=self.params_count + 2, column=1, sticky="e", padx=5, pady=30)
        self.create_button.grid(row=self.params_count + 2, column=2, sticky="w", padx=5, pady=30)

    def create(self) -> None:
        name = self.entry_name.get()
        if not name:
            show_error_dialog("Name can not be empty!")
            return

        params = [p.get() for p in self.params_entries]
        for param in params:
            if not param:
                show_error_dialog("Parameter can not be empty!")
                return

        if name.upper() in self.vertice_names:
            show_error_dialog(f"Vertex {name} already exists!")
            return

        self.vertice_names.add(name.upper())
        self.vertex(self.parent, name, params, self.check_delete_fun, self.vertice_names)
        self.destroy()
