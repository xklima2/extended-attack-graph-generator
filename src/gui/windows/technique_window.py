from __future__ import annotations

from typing import Any, Optional, Callable, TYPE_CHECKING

import customtkinter  # type: ignore

from src.gui.scrollables.countermeasure_scrollable import CountermeasureScrollable
from src.gui.scrollables.level_scrollable import LevelScrollable
from src.gui.scrollables.property_scrollable import PropertyScrollable
from src.gui.utils import show_error_dialog
from src.gui.vertices.filled_vertex import FilledVertex
from src.gui.vertices.vertex import Vertex

if TYPE_CHECKING:
    from src.gui.vertices.technique import Technique


class TechniqueWindow(customtkinter.CTkToplevel):  # type: ignore
    def __init__(self, parent: Optional[Any], technique: Optional[Technique], properties: PropertyScrollable,
                 countermeasures: CountermeasureScrollable, levels: LevelScrollable, is_edit: bool):
        super().__init__()
        if is_edit:
            assert technique is not None  # mypy does not get this -> type ignore later on
        else:
            assert parent is not None

        self.parent = parent
        self.technique = technique
        self.properties = properties
        self.countermeasures = countermeasures
        self.levels = levels
        self.is_edit = is_edit

        self.result_vertex: Optional[FilledVertex] = technique.result_vertex if is_edit else None  # type: ignore
        self.condition_count = 0
        self.conditions: list[FilledVertex] = []

        self.title("Edit technique" if is_edit else "Add new technique")
        self.geometry("600x400")

        self.columnconfigure(0, minsize=30)
        self.columnconfigure(1, weight=3)
        self.columnconfigure(2, weight=3)
        self.columnconfigure(3, weight=3)
        self.columnconfigure(4, minsize=30)
        self.rowconfigure(0, minsize=30)

        label_id = customtkinter.CTkLabel(self, text="ID:")
        label_id.grid(row=1, column=1, sticky="e", padx=5, pady=5)
        self.entry_id = customtkinter.CTkEntry(self, placeholder_text="Enter technique id")
        if is_edit:
            self.entry_id.insert(0, technique.technique_id)  # type: ignore
        self.entry_id.grid(row=1, column=2, sticky="we", padx=5, pady=5)

        name = customtkinter.CTkLabel(self, text="Name:")
        name.grid(row=2, column=1, sticky="e", padx=5, pady=5)
        self.entry_name = customtkinter.CTkEntry(self, placeholder_text="Enter technique name")
        if is_edit:
            self.entry_name.insert(0, technique.name)  # type: ignore
        self.entry_name.grid(row=2, column=2, sticky="we", padx=5, pady=5)

        res_vertex = customtkinter.CTkLabel(self, text="Result vertex:")
        res_vertex.grid(row=3, column=1, sticky="e", padx=5, pady=5)
        self.res_vertex_filled = customtkinter.CTkLabel(self, text=str(self.result_vertex) if is_edit else "None",
                                                        anchor="w")
        self.res_vertex_filled.grid(row=3, column=2, sticky="we", padx=5, pady=5)
        self.select_result_button = customtkinter.CTkButton(self, text="Select result vertex",
                                                            command=lambda: self.select_vertex(self.assign_res_ver,
                                                                                               "result vertex", True))
        self.select_result_button.grid(row=3, column=3, sticky="w", padx=5, pady=5)

        self.add_condition_button = customtkinter.CTkButton(self, text="Add condition",
                                                            command=lambda: self.select_vertex(self.add_condition,
                                                                                               "condition", False))
        self.add_condition_button.grid(row=4, column=2, sticky="we", padx=5, pady=5)

        self.ok_button = customtkinter.CTkButton(self, text="OK", command=self.ok_pressed)
        self.ok_button.grid(row=5, column=3, sticky="w", padx=5, pady=5)

        if is_edit:
            for condition in technique.conditions:  # type: ignore
                self.add_filled_condition(condition)

    def add_filled_condition(self, condition: FilledVertex) -> None:
        self.conditions.append(condition)
        self.condition_count += 1

        cond_label = customtkinter.CTkLabel(self, text="Condition:")
        cond_label.grid(row=self.condition_count + 3, column=1, sticky="e", padx=5, pady=5)
        cond_filled = customtkinter.CTkLabel(self, text=str(condition), anchor="w")
        cond_filled.grid(row=self.condition_count + 3, column=2, sticky="we", padx=5, pady=5)
        cond_delete_but: Any = (customtkinter.CTkButton(self, text="Delete",
                                                        command=lambda: self.delete_condition(condition, cond_label,
                                                                                              cond_filled,
                                                                                              cond_delete_but)))
        cond_delete_but.grid(row=self.condition_count + 3, column=3, sticky="w", padx=5, pady=5)

        self.add_condition_button.grid(row=self.condition_count + 4, column=2, sticky="we", padx=5, pady=5)
        self.ok_button.grid(row=self.condition_count + 5, column=3, sticky="w", padx=5, pady=5)

    def delete_condition(self, condition: FilledVertex, cond_label: customtkinter.CTkLabel,
                         cond_filled: customtkinter.CTkLabel, delete_but: customtkinter.CTkButton) -> None:
        self.conditions.remove(condition)
        self.condition_count -= 1
        cond_label.destroy()
        cond_filled.destroy()
        delete_but.destroy()

    def assign_res_ver(self, result_vertex: FilledVertex, window: customtkinter.CTkToplevel) -> None:
        if not result_vertex.all_params_filled():
            show_error_dialog("Empty parameter value!")
            return

        window.destroy()
        self.result_vertex = result_vertex
        self.res_vertex_filled.configure(text=str(result_vertex))

    def add_condition(self, condition: FilledVertex, window: customtkinter.CTkToplevel) -> None:
        if not condition.all_params_filled():
            show_error_dialog("Empty parameter value!")
            return
        window.destroy()
        self.conditions.append(condition)
        self.condition_count += 1

        cond_label = customtkinter.CTkLabel(self, text="Condition:")
        cond_label.grid(row=self.condition_count + 3, column=1, sticky="e", padx=5, pady=5)
        cond_filled = customtkinter.CTkLabel(self, text=str(condition), anchor="w")
        cond_filled.grid(row=self.condition_count + 3, column=2, sticky="we", padx=5, pady=5)

        self.add_condition_button.grid(row=self.condition_count + 4, column=2, sticky="we", padx=5, pady=5)
        self.ok_button.grid(row=self.condition_count + 5, column=3, sticky="w", padx=5, pady=5)

    @staticmethod
    def fill_vertex(vertex: Vertex, window: customtkinter.CTkToplevel,
                    assign_fun: Callable[[FilledVertex, customtkinter.CTkToplevel], None], title: str) -> None:
        for widget in window.winfo_children():
            widget.destroy()
        window.title(f"Fill {title} attributes")

        scrollable = customtkinter.CTkScrollableFrame(window, width=550, height=350)
        scrollable.pack()

        entries = []
        for param in vertex.params:
            frame = customtkinter.CTkFrame(scrollable, border_width=0, width=350)
            label = customtkinter.CTkLabel(frame, text=param + ":", width=80, anchor="e")
            label.grid(row=1, column=1, sticky="e", padx=5, pady=5)
            param_entry = customtkinter.CTkEntry(frame, placeholder_text="Enter parameter value")
            param_entry.grid(row=1, column=2, sticky="w", padx=5, pady=5)
            entries.append(param_entry)
            frame.pack()

        ok_button = customtkinter.CTkButton(window, text="OK", command=lambda: assign_fun(
            FilledVertex(vertex, [entry.get() for entry in entries]), window))
        ok_button.pack()

    def select_vertex(self, assign_fun: Callable[[FilledVertex, customtkinter.CTkToplevel], None], title: str,
                      levels_only: bool) -> None:
        window = customtkinter.CTkToplevel()
        window.title("Select " + title)
        window.geometry("600x400")

        scrollable = customtkinter.CTkScrollableFrame(window, width=550, height=350)
        scrollable.pack()

        vertices = self.levels.winfo_children()
        if not levels_only:
            vertices += self.properties.winfo_children() + self.countermeasures.winfo_children()
        for vertex in vertices:
            assert isinstance(vertex, Vertex)
            frame = customtkinter.CTkFrame(scrollable, border_width=0, width=10)
            frame.columnconfigure(1, weight=1)
            frame.columnconfigure(2, minsize=30)
            label = customtkinter.CTkLabel(frame, text=str(vertex), width=80, anchor="w")
            label.grid(row=1, column=1, sticky="w", padx=5, pady=5)
            select = customtkinter.CTkButton(frame, text="Select",
                                             command=lambda v=vertex: self.fill_vertex(v, window, assign_fun, title))
            select.grid(row=1, column=2, sticky="e", padx=5, pady=5)
            frame.pack(expand=True, fill="both")

    def ok_pressed(self) -> None:
        technique_id = self.entry_id.get()
        if not technique_id:
            show_error_dialog("Empty id!")
            return

        name = self.entry_name.get()
        if not name:
            show_error_dialog("Empty name!")
            return

        if not self.result_vertex:
            show_error_dialog("No result vertex selected!")
            return
        if self.condition_count == 0:
            show_error_dialog("No conidtions selected!")
            return

        if self.is_edit:
            self.technique.change_id(technique_id)  # type: ignore
            self.technique.change_name(name)  # type: ignore
            self.technique.result_vertex = self.result_vertex  # type: ignore
            self.technique.conditions = self.conditions  # type: ignore
        else:
            from src.gui.vertices.technique import Technique
            Technique(self.parent, technique_id, name, self.result_vertex, self.conditions,
                      self.properties, self.countermeasures, self.levels)

        self.destroy()
