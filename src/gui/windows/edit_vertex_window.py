from __future__ import annotations

from typing import TYPE_CHECKING

import customtkinter  # type: ignore

from src.gui.utils import show_error_dialog

if TYPE_CHECKING:
    from src.gui.vertices.vertex import Vertex


class EditVertexVindow(customtkinter.CTkToplevel):  # type: ignore
    def __init__(self, vertex: Vertex, vertice_names: set[str]) -> None:
        super().__init__()
        self.vertex = vertex
        self.vertice_names = vertice_names

        self.params_count = 0
        self.params_entries: list[customtkinter.CTkEntry] = []

        self.title(f"Edit {self.vertex.type_str}")
        self.geometry("600x400")

        self.columnconfigure(0, minsize=30)
        self.columnconfigure(1, weight=3)
        self.columnconfigure(2, weight=3)
        self.columnconfigure(3, minsize=30)
        self.rowconfigure(0, minsize=30)

        name = customtkinter.CTkLabel(self, text="Name:")
        name.grid(row=1, column=1,  sticky="e", padx=5, pady=5)
        self.entry_name = customtkinter.CTkEntry(self)
        self.entry_name.insert(0, self.vertex.name)
        self.entry_name.grid(row=1, column=2, sticky="w", padx=5, pady=5)

        self.ok_button = customtkinter.CTkButton(self, text="OK", command=self.ok_pressed)

        for param in vertex.params:
            self.add_param(param)

    def add_param(self, param: str) -> None:
        self.params_count += 1
        param_lab = customtkinter.CTkLabel(self, text="Parameter:")
        param_lab.grid(row=self.params_count + 1, column=1, sticky="e", padx=5, pady=5)
        entry_param = customtkinter.CTkEntry(self)
        entry_param.insert(0, param)
        entry_param.grid(row=self.params_count + 1, column=2,  sticky="w", padx=5, pady=5)
        self.params_entries.append(entry_param)
        self.ok_button.grid(row=self.params_count + 2, column=2, sticky="w", padx=5, pady=30)

    def ok_pressed(self) -> None:
        name = self.entry_name.get()
        if not name:
            show_error_dialog("Name can not be empty!")

        params = [p.get() for p in self.params_entries]
        for param in params:
            if not param:
                show_error_dialog("Parameter can not be empty!")
                return

        if name.upper() in self.vertice_names:
            show_error_dialog(f"Vertex {name} already exists!")
            return

        self.vertice_names.remove(self.vertex.name.upper())
        self.vertice_names.add(name.upper())
        self.vertex.change_name(name)
        assert len(self.vertex.params) == len(params)
        self.vertex.change_params(params)

        self.destroy()
