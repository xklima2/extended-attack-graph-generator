from typing import Any, Callable, Optional

from src.gui.vertices.level import Level
from src.gui.scrollables.vertices_scrollable import VerticesScrollable


class LevelScrollable(VerticesScrollable):
    def __init__(self, master: Any, check_fun: Callable[[str], Optional[str]], vertice_names: set[str]):
        super().__init__(master, Level, check_fun, vertice_names)
