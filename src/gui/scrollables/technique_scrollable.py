from typing import Any

from src.gui.scrollables.scrollable import Scrollable
from src.gui.scrollables.countermeasure_scrollable import CountermeasureScrollable
from src.gui.scrollables.property_scrollable import PropertyScrollable
from src.gui.scrollables.level_scrollable import LevelScrollable
from src.gui.windows.technique_window import TechniqueWindow


class TechniqueScrollable(Scrollable):
    def __init__(self, master: Any, properties: PropertyScrollable, countermeasures: CountermeasureScrollable,
                 levels: LevelScrollable):
        super().__init__(master)
        self.properties = properties
        self.countermeasures = countermeasures
        self.levels = levels

    def add(self) -> None:
        TechniqueWindow(self, None, self.properties, self.countermeasures, self.levels, False)
