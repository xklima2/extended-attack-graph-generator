from typing import Any, Callable, Optional

from src.gui.vertices.property import Property
from src.gui.scrollables.vertices_scrollable import VerticesScrollable


class PropertyScrollable(VerticesScrollable):
    def __init__(self, master: Any, check_fun: Callable[[str], Optional[str]], vertice_names: set[str]):
        super().__init__(master, Property, check_fun, vertice_names)
