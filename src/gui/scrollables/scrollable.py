from abc import abstractmethod, ABC
from typing import Any

import customtkinter  # type: ignore


class Scrollable(customtkinter.CTkScrollableFrame, ABC):  # type: ignore
    def __init__(self, master: Any):
        super().__init__(master, width=750, height=400)
        self.bind("<Button-4>", lambda e: self._parent_canvas.yview("scroll", -1, "units"))
        self.bind("<Button-5>", lambda e: self._parent_canvas.yview("scroll", 1, "units"))

    @abstractmethod
    def add(self) -> None:
        pass
