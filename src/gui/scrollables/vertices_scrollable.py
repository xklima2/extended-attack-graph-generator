from typing import Any, Type, Optional, Callable

from src.gui.scrollables.scrollable import Scrollable
from src.gui.vertices.vertex import Vertex
from src.gui.windows.add_vertex_window import AddVertexWindow


class VerticesScrollable(Scrollable):
    def __init__(self, master: Any, vertex: Type[Vertex], check_delete_fun: Callable[[str], Optional[str]],
                 vertice_names: set[str]):
        super().__init__(master)
        self.vertex = vertex
        self.check_relete_fun = check_delete_fun
        self.vertice_names = vertice_names

    def add(self) -> None:
        AddVertexWindow(self.vertex, self, self.check_relete_fun, self.vertice_names)
