import json
from collections import defaultdict
from tkinter import filedialog
from typing import Optional, Any

import customtkinter  # type: ignore

from src.gui.scrollables.countermeasure_scrollable import CountermeasureScrollable
from src.gui.scrollables.level_scrollable import LevelScrollable
from src.gui.scrollables.property_scrollable import PropertyScrollable
from src.gui.scrollables.scrollable import Scrollable
from src.gui.scrollables.technique_scrollable import TechniqueScrollable
from src.gui.utils import show_error_dialog
from src.gui.vertices.countermeasure import Countermeasure
from src.gui.vertices.filled_vertex import FilledVertex
from src.gui.vertices.level import Level
from src.gui.vertices.property import Property
from src.gui.vertices.technique import Technique
from src.gui.vertices.vertex import Vertex


class Ruleset(customtkinter.CTk):  # type: ignore
    def __init__(self) -> None:
        super().__init__()
        self.vertice_names: set[str] = set()

        self.title("Kill Chain Attack Graph generator")
        self.geometry("1000x600")

        self.columnconfigure(0, minsize=30)
        self.columnconfigure(1, weight=1)
        self.columnconfigure(30, minsize=30)
        self.rowconfigure(0, minsize=10)
        self.rowconfigure(1, weight=11)
        self.rowconfigure(2, minsize=30)
        self.rowconfigure(3, minsize=10)

        self.tabview = customtkinter.CTkTabview(self)
        self.tabview.grid(row=1, column=1, padx=10, pady=10, sticky="ns")

        self.property_tab = self.tabview.add("Property")
        self.countermasure_tab = self.tabview.add("Countermeasure")
        self.level_tab = self.tabview.add("Level")
        self.technique_tab = self.tabview.add("Technique")

        self.property_scrollable = PropertyScrollable(self.property_tab, self.vertex_used_in_technique,
                                                      self.vertice_names)
        self.property_scrollable.pack()
        self.countermasure_scrollable = CountermeasureScrollable(self.countermasure_tab, self.vertex_used_in_technique,
                                                                 self.vertice_names)
        self.countermasure_scrollable.pack()
        self.level_scrollable = LevelScrollable(self.level_tab, self.vertex_used_in_technique, self.vertice_names)
        self.level_scrollable.pack()
        self.technique_scrollable = TechniqueScrollable(self.technique_tab, self.property_scrollable,
                                                        self.countermasure_scrollable, self.level_scrollable)
        self.technique_scrollable.pack()

        buttons_frame = customtkinter.CTkFrame(self)
        buttons_frame.grid(row=2, column=1, padx=10, pady=10, sticky="n")
        add_button = customtkinter.CTkButton(buttons_frame, text="Add", command=self.add)
        add_button.grid(row=1, column=0, padx=5, pady=5)

        load_button = customtkinter.CTkButton(buttons_frame, text="Load rules from file", command=self.load_rules)
        load_button.grid(row=1, column=1, padx=5, pady=5)

        save_button = customtkinter.CTkButton(buttons_frame, text="Save rules to file", command=self.save_rules)
        save_button.grid(row=1, column=2, padx=5, pady=5)

        delete_button = customtkinter.CTkButton(buttons_frame, text="Delete everything", command=self.delete_all)
        delete_button.grid(row=1, column=3, padx=5, pady=5)

    def vertex_used_in_technique(self, vertex_name: str) -> Optional[str]:
        for technique in self.technique_scrollable.winfo_children():
            assert isinstance(technique, Technique)
            if technique.vertex_used(vertex_name):
                return technique.technique_id
        return None

    def delete_all(self) -> None:
        for prop in self.property_scrollable.winfo_children():
            prop.destroy()
        for countermeasure in self.countermasure_scrollable.winfo_children():
            countermeasure.destroy()
        for level in self.level_scrollable.winfo_children():
            level.destroy()
        for technique in self.technique_scrollable.winfo_children():
            technique.destroy()
        self.vertice_names.clear()

    def add(self) -> None:
        selected_tab = self.tabview.get()
        scrollables: dict[str, Scrollable] = {
            "Property": self.property_scrollable,
            "Countermeasure": self.countermasure_scrollable,
            "Level": self.level_scrollable,
            "Technique": self.technique_scrollable
        }
        scrollables[selected_tab].add()

    @staticmethod
    def open_ruleset() -> Optional[str]:
        filename = filedialog.askopenfilename()
        if not filename:
            return None
        if not filename.endswith(".json"):
            show_error_dialog("You need to select a JSON file")
            return None
        return filename

    @staticmethod
    def validate_ruleset(filename: str) -> Optional[dict[str, Any]]:
        try:
            with open(filename, "r") as file:
                ruleset = json.loads(file.read())
        except json.decoder.JSONDecodeError:
            show_error_dialog("Invalid JSON file!")
            return None

        if not isinstance(ruleset, dict):
            show_error_dialog("Invalid ruleset format!")
            return None

        if not (all(k in ruleset for k in ["PROPERTY", "COUNTERMEASURE", "LEVEL", "TECHNIQUE"]) and len(ruleset) == 4):
            show_error_dialog("Ivalid ruleset format! Properties, countermeasures, "
                              "levels and techniques must be defined.")
            return None

        for vertex in ["PROPERTY", "COUNTERMEASURE", "LEVEL"]:
            for name, params in ruleset[vertex].items():
                if not isinstance(name, str) or not isinstance(params, list):
                    show_error_dialog(f"Invalid ruleset format! Error in {vertex.lower()} {name}")
                    return None

        for result_vertex, techniques in ruleset["TECHNIQUE"].items():
            if not isinstance(result_vertex, str) or not isinstance(techniques, list):
                show_error_dialog(f"Invalid ruleset format! Problem with {result_vertex}")
                return None
            for technique in techniques:
                if not isinstance(technique, dict):
                    show_error_dialog(f"Invalid ruleset format! Invalid technique with {result_vertex} result vertex")
                    return None
                if not all(k in technique for k in ["id", "name", "conditions", "resultVectorAttributes"]):
                    show_error_dialog(f"Invalid ruleset format! Invalid technique with {result_vertex} result vertex")
                    return None
                if not (isinstance(technique["id"], str) and isinstance(technique["name"], str) and
                        isinstance(technique["conditions"], list)
                        and isinstance(technique["resultVectorAttributes"], list)):
                    show_error_dialog(f"Invalid ruleset format! Problem with technique with {technique['id']} id.")
                    return None
                for condition in technique["conditions"]:
                    if not isinstance(condition, dict):
                        show_error_dialog(
                            f"Invalid ruleset format! Problem with conditions in {technique['id']} technique")
                        return None
                    if not len(condition) == 1 or not isinstance(list(condition.values())[0], list):
                        show_error_dialog(
                            f"Invalid ruleset format! Problem with conditions in {technique['id']} technique")
                        return None

        return ruleset

    def load_rules(self) -> None:
        filename = self.open_ruleset()
        if not filename:
            return
        ruleset = self.validate_ruleset(filename)
        if not ruleset:
            return

        self.delete_all()

        vertices: dict[str, Vertex] = {}
        for name, params in ruleset["PROPERTY"].items():
            if name in vertices:
                show_error_dialog(f"Multiple vertices with '{name}' name detected in the input file. "
                                  f"Skipping '{name}' with '{params}' parameters")
                continue
            vertices[name] = Property(self.property_scrollable, name, params, self.vertex_used_in_technique,
                                      self.vertice_names)

        for name, params in ruleset["COUNTERMEASURE"].items():
            if name in vertices:
                show_error_dialog(f"Multiple vertices with '{name}' name detected in the input file. "
                                  f"Skipping '{name}' with '{params}' parameters")
                continue
            vertices[name] = Countermeasure(self.countermasure_scrollable, name, params, self.vertex_used_in_technique,
                                            self.vertice_names)

        for name, params in ruleset["LEVEL"].items():
            if name in vertices:
                show_error_dialog(f"Multiple vertices with '{name}' name detected in the input file. "
                                  f"Skipping '{name}' with '{params}' parameters")
            vertices[name] = Level(self.level_scrollable, name, params, self.vertex_used_in_technique,
                                   self.vertice_names)

        for res_vertex_name, techniques in ruleset["TECHNIQUE"].items():
            if res_vertex_name not in vertices:
                show_error_dialog(f"Can't create techniques with '{res_vertex_name}' "
                                  "result vertex because it is not defined in the input file")
                continue
            res_vertex = vertices[res_vertex_name]
            for technique in techniques:
                filled_res_vertex = FilledVertex(res_vertex, technique["resultVectorAttributes"])
                conditions: list[FilledVertex] = []
                for condition in technique["conditions"]:
                    cond_name, cond_params = next(iter(condition.items()))
                    if cond_name not in vertices:
                        show_error_dialog(f"Can't create condition with '{cond_name}' name in {technique['name']} "
                                          "technique because it is not defined in the input file")
                        continue
                    conditions.append(FilledVertex(vertices[cond_name], cond_params))
                Technique(self.technique_scrollable, technique["id"], technique["name"], filled_res_vertex, conditions,
                          self.property_scrollable, self.countermasure_scrollable, self.level_scrollable)

    def save_rules(self) -> None:
        filename = filedialog.asksaveasfilename()
        if not filename:
            return
        filename = filename.strip().split(".")[0] + ".json"

        properties = {}
        for prop in self.property_scrollable.winfo_children():
            assert isinstance(prop, Property)
            properties[prop.name] = prop.params

        countermeasures = {}
        for counter in self.countermasure_scrollable.winfo_children():
            assert isinstance(counter, Countermeasure)
            countermeasures[counter.name] = counter.params

        levels = {}
        for level in self.level_scrollable.winfo_children():
            assert isinstance(level, Level)
            levels[level.name] = level.params

        techniques = defaultdict(list)
        for technique in self.technique_scrollable.winfo_children():
            assert isinstance(technique, Technique)
            techniques[technique.get_result_vertex_name()].append({
                "id": technique.technique_id,
                "name": technique.name,
                "resultVectorAttributes": technique.get_result_vertex_attributes(),
                "conditions": technique.get_conditions()
            })

        ruleset = {
            "PROPERTY": properties,
            "COUNTERMEASURE": countermeasures,
            "LEVEL": levels,
            "TECHNIQUE": techniques
        }

        with open(filename, "w") as file:
            json.dump(ruleset, file, indent=4)


def main() -> None:
    customtkinter.set_appearance_mode("System")
    customtkinter.set_default_color_theme("blue")

    app = Ruleset()
    app.mainloop()


if __name__ == '__main__':
    main()
