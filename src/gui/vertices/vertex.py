from typing import Any, Optional, Callable

import customtkinter  # type: ignore

from src.gui.utils import show_error_dialog
from src.gui.windows.edit_vertex_window import EditVertexVindow


class Vertex(customtkinter.CTkFrame):  # type: ignore
    type_str = ""

    def __init__(self, master: Any, name: str, params: list[str], check_delete_fun: Callable[[str], Optional[str]],
                 vertice_names: set[str]):
        super().__init__(master, border_width=2, border_color="gray", width=30)
        self.name = name
        self.params = params
        self.check_delete_fun = check_delete_fun
        self.vertice_names = vertice_names
        self.vertice_names.add(name.upper())

        self.columnconfigure(1, weight=1)
        self.columnconfigure(2, weight=1)

        self.name_l = customtkinter.CTkLabel(self, text="Name:")
        self.name_l.grid(row=0, column=0, padx=20, pady=10, sticky="e")
        self.name_r = customtkinter.CTkLabel(self, text=name)
        self.name_r.grid(row=0, column=1, padx=0, pady=10, sticky="w")

        self.params_l = customtkinter.CTkLabel(self, text="Params:")
        self.params_l.grid(row=1, column=0, padx=20, pady=10, sticky="e")
        self.params_r = customtkinter.CTkLabel(self, text=", ".join(params))
        self.params_r.grid(row=1, column=1, padx=0, pady=10, sticky="w")

        self.delete_button = customtkinter.CTkButton(self, text="Delete", command=self.delete)
        self.delete_button.grid(row=1, column=2, padx=20, pady=10, sticky="e")

        self.edit_button = customtkinter.CTkButton(self, text="Edit", command=self.edit)
        self.edit_button.grid(row=0, column=2, padx=20, pady=10, sticky="e")

        self.pack(pady=5, padx=50, fill="x")

    def __str__(self) -> str:
        return self.name + ": " + str(self.params)

    def delete(self) -> None:
        used_in = self.check_delete_fun(self.name)
        if not used_in:
            self.destroy()
        else:
            show_error_dialog(f"Can't delete {self.name} bacause it is used in technique {used_in}")

    def edit(self) -> None:
        EditVertexVindow(self, self.vertice_names)

    def change_name(self, new_name: str) -> None:
        self.name = new_name
        self.name_r.configure(text=new_name)

    def change_params(self, new_params: list[str]) -> None:
        self.params = new_params
        self.params_r.configure(text=", ".join(new_params))
