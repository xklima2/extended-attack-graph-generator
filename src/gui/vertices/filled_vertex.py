from src.gui.vertices.vertex import Vertex


class FilledVertex:
    def __init__(self, vertex_definition: Vertex, params_values: list[str]):
        assert len(vertex_definition.params) == len(params_values)
        self.vertex_definition = vertex_definition
        self.params_values = params_values

    def all_params_filled(self) -> bool:
        return all(self.params_values)

    def __str__(self) -> str:
        return self.vertex_definition.name + ": " + str(self.params_values)
