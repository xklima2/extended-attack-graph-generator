from typing import Any

import customtkinter  # type: ignore

from src.gui.scrollables.countermeasure_scrollable import CountermeasureScrollable
from src.gui.scrollables.level_scrollable import LevelScrollable
from src.gui.scrollables.property_scrollable import PropertyScrollable
from src.gui.vertices.filled_vertex import FilledVertex
from src.gui.windows.technique_window import TechniqueWindow


class Technique(customtkinter.CTkFrame):  # type: ignore
    def __init__(self, master: Any, technique_id: str, name: str, result_vertex: FilledVertex,
                 conditions: list[FilledVertex], properties: PropertyScrollable,
                 countermeasures: CountermeasureScrollable, levels: LevelScrollable):
        super().__init__(master, border_width=2, border_color="gray", width=700)
        self.technique_id = technique_id
        self.name = name
        self.result_vertex = result_vertex
        self.conditions = conditions

        self.properties = properties
        self.countermeasures = countermeasures
        self.levels = levels

        self.columnconfigure(1, weight=1)
        self.columnconfigure(2, weight=1)

        self.id_l = customtkinter.CTkLabel(self, text="ID:")
        self.id_l.grid(row=0, column=0, padx=20, pady=10, sticky="e")
        self.id_r = customtkinter.CTkLabel(self, text=technique_id)
        self.id_r.grid(row=0, column=1, padx=0, pady=10, sticky="w")

        self.name_l = customtkinter.CTkLabel(self, text="Name:")
        self.name_l.grid(row=1, column=0, padx=20, pady=10, sticky="e")
        self.name_r = customtkinter.CTkLabel(self, text=name)
        self.name_r.grid(row=1, column=1, padx=0, pady=10, sticky="w")

        self.delete_button = customtkinter.CTkButton(self, text="Delete", command=self.delete)
        self.delete_button.grid(row=1, column=2, padx=20, pady=10, sticky="e")

        self.edit_button = customtkinter.CTkButton(self, text="Edit", command=self.edit)
        self.edit_button.grid(row=0, column=2, padx=20, pady=10, sticky="e")

        self.pack(pady=5, padx=50, fill="x")

    def delete(self) -> None:
        self.destroy()

    def edit(self) -> None:
        TechniqueWindow(None, self, self.properties, self.countermeasures, self.levels, True)

    def change_id(self, new_id: str) -> None:
        self.technique_id = new_id
        self.id_r.configure(text=new_id)

    def change_name(self, new_name: str) -> None:
        self.name = new_name
        self.name_r.configure(text=new_name)

    def get_result_vertex_name(self) -> str:
        return self.result_vertex.vertex_definition.name

    def get_result_vertex_attributes(self) -> list[str]:
        return self.result_vertex.params_values

    def get_conditions(self) -> list[dict[str, list[str]]]:
        return [{condition.vertex_definition.name: condition.params_values} for condition in self.conditions]

    def vertex_used(self, vertex_name: str) -> bool:
        if self.result_vertex.vertex_definition.name == vertex_name:
            return True
        for condition in self.conditions:
            if condition.vertex_definition.name == vertex_name:
                return True
        return False
