from typing import Any, Optional, Callable

from src.gui.vertices.vertex import Vertex


class Countermeasure(Vertex):
    type_str = "countermeasure"

    def __init__(self, master: Any, name: str, params: list[str], check_delete_fun: Callable[[str], Optional[str]],
                 vertices_names: set[str]):
        super().__init__(master, name, params, check_delete_fun, vertices_names)
