import customtkinter  # type: ignore


def show_error_dialog(message: "str") -> None:
    error_dialog = customtkinter.CTkToplevel()
    error_dialog.title("Error")

    label = customtkinter.CTkLabel(error_dialog, text=message)
    label.pack(padx=20, pady=10)

    ok_button = customtkinter.CTkButton(error_dialog, text="OK", command=error_dialog.destroy)
    ok_button.pack(pady=5)
