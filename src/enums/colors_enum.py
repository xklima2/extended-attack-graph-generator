from enum import Enum


class Color(Enum):
    WHITE = 1
    GRAY = 2
    BLACK = 3
