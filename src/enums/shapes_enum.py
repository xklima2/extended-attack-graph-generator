from enum import Enum


class Shape(str, Enum):
    PROPERTY = "octagon"
    LEVEL = "ellipse"
    TECHNIQUE = "rectangle"
    COUNTERMEASURE = "diamond"
    ATTACK_GOAL = "triangle"
