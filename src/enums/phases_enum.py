from enum import Enum


class Phase(str, Enum):
    RECONNAISSANCE = "Reconnaissance"
    RESOURCE_DEVELOPMENT = "Resource Development"
    CREDENTIAL_ACCESS = "Credential Access"
    INITIAL_ACCESS = "Initial Access"
    PERSISTENCE = "Persistence"
    DEFENSE_EVASION = "Defense Evasion"
    EXECUTION = "Execution"
    PRIVILEGE_ESCALATION = "Privilege Escalation"
    DISCOVERY = "Discovery"
    LATERAL_MOVEMENT = "Lateral Movement"
    COLLECTION = "Collection"
    COMMAND_AND_CONTROL = "Command and Control"
    EXFILTRATION = "Exfiltration"
    IMPACT = "Impact"

    def __repr__(self) -> str:
        return f'"{self.value}"'


def get_kill_chain_phases(technique_id: str) -> set[Phase]:
    """
    Returns set of possible kill chain phases for technique with given ID

    @param technique_id: str
        ID of technique
    @return: kill_chain_phases: set[Phase]
        Set of possible kill chain phases
    """
    kill_chain_phases = {
        'T1594': {Phase.RECONNAISSANCE},  # Search Victim-Owned Websites
        'T1595': {Phase.RECONNAISSANCE},  # Active Scanning
        'T1190': {Phase.INITIAL_ACCESS},  # Exploit Public-Facing Application
        'T1133': {Phase.INITIAL_ACCESS},  # External Remote Services
        'T1566.002': {Phase.INITIAL_ACCESS},  # Spearphishing Link
        'T1566.001': {Phase.INITIAL_ACCESS},  # Spearphishing Attachment
        'T1078': {Phase.INITIAL_ACCESS, Phase.PRIVILEGE_ESCALATION},  # T1078 - Valid Accounts
        'T1078.001': {Phase.INITIAL_ACCESS, Phase.PRIVILEGE_ESCALATION},  # T1078.001: Default accounts
        'T1059.008': {Phase.EXECUTION},  # T1059.008: Command and Scripting Interpreter - Network Device CLI
        'T1203': {Phase.EXECUTION},  # Exploitation for Client Execution
        'T1204.001': {Phase.EXECUTION},  # User Execution - Malicious link
        'T1204.002': {Phase.EXECUTION},  # User Execution - Malicious File
        'T1068': {Phase.PRIVILEGE_ESCALATION},  # Exploitation for Privilege Escalation
        'T1110': {Phase.CREDENTIAL_ACCESS},  # Brute Force
        'T1557': {Phase.CREDENTIAL_ACCESS, Phase.COLLECTION},  # Man in the Middle
        'T1040': {Phase.CREDENTIAL_ACCESS, Phase.DISCOVERY},  # Network Sniffing
        'T1046': {Phase.DISCOVERY, Phase.RECONNAISSANCE},  # Network Service Scanning
        'T1049': {Phase.DISCOVERY},  # System Network Connections Discovery
        'T1018': {Phase.DISCOVERY},  # Remote System Discovery
        'T1083': {Phase.DISCOVERY},  # File and Directory Discovery
        'T1210': {Phase.LATERAL_MOVEMENT},  # Exploitation of Remote Services
        'T1534': {Phase.LATERAL_MOVEMENT},  # Internal Spearphishing
        'T1563.001': {Phase.LATERAL_MOVEMENT},  # Remote Service Session Hijacking - SSH Hijacking
        'T1021': {Phase.LATERAL_MOVEMENT},  # Remote Services
        'T1114': {Phase.COLLECTION},  # Email Collection
        'T1185': {Phase.COLLECTION},  # Man in the Browser
        'T1005': {Phase.COLLECTION},  # Data from Local System
        'T1071': {Phase.COMMAND_AND_CONTROL},  # Application Layer Protocol
        'T1573': {Phase.COMMAND_AND_CONTROL},  # Encrypted Channel
        'T1095': {Phase.COMMAND_AND_CONTROL},  # Non-Application Layer Protocol
        'T1571': {Phase.COMMAND_AND_CONTROL},  # Non-Standard Port
        'T1219': {Phase.COMMAND_AND_CONTROL},  # Remote Access Software
        'T1090': {Phase.COMMAND_AND_CONTROL},  # Proxy
        'T1102': {Phase.COMMAND_AND_CONTROL},  # Web Service
        'T1048': {Phase.EXFILTRATION},  # Exfiltration Over Alternative Protocol
        'T1041': {Phase.EXFILTRATION},  # Exfiltration Over C2 Channel
        'T1567': {Phase.EXFILTRATION},  # Exfiltration Over Web Service
        'T1499.004': {Phase.IMPACT},  # Endpoint Denial of Service - Application or System Exploitation
        'T1498': {Phase.IMPACT},  # Network Denial of Service
        'T1489': {Phase.IMPACT},  # Service Stop
        'T1486': {Phase.IMPACT},  # Data Encrypted for Impact
        'T1565.001': {Phase.IMPACT},  # Data Manipulation - Stored Data Manipulation
        'T1485': {Phase.IMPACT},  # Data Destruction
        'T1562.001': {Phase.DEFENSE_EVASION},  # Impair Defenses: Disable or Modify Tools
        'T1112': {Phase.DEFENSE_EVASION},  # Modify Registry
        'T1070.004': {Phase.DEFENSE_EVASION},  # File Deletion
        'T1548.003': {Phase.PRIVILEGE_ESCALATION, Phase.DEFENSE_EVASION},  # Sudo and Sudo Caching
        'T1543.003': {Phase.PERSISTENCE, Phase.PRIVILEGE_ESCALATION},  # Windows Service
    }

    return kill_chain_phases[technique_id]


def get_successor_phases(phase: Phase) -> set[Phase]:
    """
    Returns set of possible successor phases for given Phase, corresponding to partial order of phases

    @param phase: Phase
        Phase to give possible successor phases for
    @return: successor_phases: set[Phase]
        Set of possible successor phases
    """
    successor_phases: dict[Phase, set[Phase]] = {
        Phase.RECONNAISSANCE: {Phase.CREDENTIAL_ACCESS, Phase.INITIAL_ACCESS, Phase.EXECUTION,
                               Phase.PRIVILEGE_ESCALATION, Phase.PERSISTENCE, Phase.DEFENSE_EVASION,
                               Phase.DISCOVERY, Phase.LATERAL_MOVEMENT, Phase.COLLECTION, Phase.COMMAND_AND_CONTROL,
                               Phase.IMPACT, Phase.EXFILTRATION},
        Phase.RESOURCE_DEVELOPMENT: {Phase.CREDENTIAL_ACCESS, Phase.INITIAL_ACCESS, Phase.EXECUTION,
                                     Phase.PRIVILEGE_ESCALATION, Phase.PERSISTENCE, Phase.DEFENSE_EVASION,
                                     Phase.DISCOVERY, Phase.LATERAL_MOVEMENT, Phase.COLLECTION,
                                     Phase.COMMAND_AND_CONTROL, Phase.IMPACT, Phase.EXFILTRATION},
        Phase.CREDENTIAL_ACCESS: {Phase.COLLECTION, Phase.COMMAND_AND_CONTROL, Phase.IMPACT, Phase.EXFILTRATION},
        Phase.INITIAL_ACCESS: {Phase.EXECUTION, Phase.PRIVILEGE_ESCALATION, Phase.PERSISTENCE,
                               Phase.DEFENSE_EVASION, Phase.CREDENTIAL_ACCESS, Phase.DISCOVERY,
                               Phase.LATERAL_MOVEMENT, Phase.COLLECTION, Phase.COMMAND_AND_CONTROL, Phase.IMPACT,
                               Phase.EXFILTRATION},
        Phase.PERSISTENCE: {Phase.COLLECTION, Phase.COMMAND_AND_CONTROL, Phase.IMPACT, Phase.EXFILTRATION},
        Phase.DEFENSE_EVASION: {Phase.COLLECTION, Phase.COMMAND_AND_CONTROL, Phase.IMPACT, Phase.EXFILTRATION},
        Phase.EXECUTION: {Phase.COLLECTION, Phase.COMMAND_AND_CONTROL, Phase.IMPACT, Phase.EXFILTRATION},
        Phase.PRIVILEGE_ESCALATION: {Phase.COLLECTION, Phase.COMMAND_AND_CONTROL, Phase.IMPACT, Phase.EXFILTRATION},
        Phase.DISCOVERY: {Phase.LATERAL_MOVEMENT, Phase.COLLECTION, Phase.COMMAND_AND_CONTROL, Phase.IMPACT,
                          Phase.EXFILTRATION},
        Phase.LATERAL_MOVEMENT: {Phase.COLLECTION, Phase.COMMAND_AND_CONTROL, Phase.IMPACT, Phase.EXFILTRATION},
        Phase.COLLECTION: {Phase.COMMAND_AND_CONTROL, Phase.EXFILTRATION},
        Phase.COMMAND_AND_CONTROL: {Phase.EXFILTRATION},
        Phase.EXFILTRATION: set(),
        Phase.IMPACT: set()
    }

    return successor_phases[phase]
