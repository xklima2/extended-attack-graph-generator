from typing import Optional


class Arguments:
    """
    Class containing all the arguments used to generate kill chain attack graph

    Attributes:
        input_file_path (str): Path to the input file
        ruleset_path (str): Path to the ruleset file
        output_path (str): Location of the generated file
        show_graph (bool): Whether to show the generated graph
        generate_input (bool): Whether to generate input
        syslog_file_path (str, optional): Path to the syslog file
        ipflow_file_path (str, optional): Path to the ipflow file
        attack_goal_file_path (str, optional): Path to the attack goal file
        start_timestamp (int, optional): Start timestamp to filter logs
        end_timestamp (int, optional): End timestamp to filter logs
        ns_min (int): Minimum of hostname - port pair occurrences to generate networkSerice attribute
    """
    def __init__(self, input_file_path: str, ruleset_path: str, output_path: str = "output", show_graph: bool = False,
                 generate_input: bool = False, syslog_fp: Optional[str] = None, ipflow_fp: Optional[str] = None,
                 attack_goal_file_path: Optional[str] = None, start_timestamp: Optional[int] = None,
                 end_timestamp: Optional[int] = None, ns_min: int = 1):
        """
               Initializes the Arguments object.

               @param input_file_path: Path to the input file
               @param ruleset_path: Path to the ruleset file
               @param output_path: Location of the generated file
               @param show_graph: Whether to show the generated graph
               @param generate_input: Whether to generate inputK
               @param syslog_fp: Path to the syslog file
               @param ipflow_fp: Path to the ipflow file
               @param attack_goal_file_path: Path to the attack goal file
               @param start_timestamp: Start timestampto filter logs
               @param end_timestamp: End timestampto filter logs
               @param ns_min: Minimum of hostname - port pair occurrences to generate networkSerice attribute
               """
        self.input_file_path = input_file_path
        self.ruleset_path = ruleset_path
        self.output_path = output_path
        self.show_graph = show_graph
        self.generate_input = generate_input
        self.syslog_file_path = syslog_fp
        self.ipflow_file_path = ipflow_fp
        self.attack_goal_file_path = attack_goal_file_path
        self.start_timestamp = start_timestamp
        self.end_timestamp = end_timestamp
        self.ns_min = ns_min

        if self.generate_input:
            assert self.syslog_file_path is not None
            assert self.ipflow_file_path is not None
            assert self.attack_goal_file_path is not None
