import json
from typing import Optional
from copy import deepcopy
from collections import defaultdict

from src.vertices.vertex import Vertex
from src.vertices.level import Level
from src.vertices.countermeasure import Countermeasure
from src.vertices.property import Property
from src.vertices.technique import Technique


class InputParser:
    """Parses JSON configuration and input files
    """

    def parse_configuration_file(self, file: str) -> tuple[dict[str, Vertex], dict[str, list[Technique]]]:
        """Parses JSON with configurations

        Parameters
        ----------
        file : str
            file with configuration

        Returns
        -------
        tuple[dict[str, Vertex], dict[str, list[Technique]]]
            all the possible vertices and techniques in the graph
        """
        predicates: dict[str, Vertex] = {}
        ruleset: dict[str, list[Technique]] = defaultdict(list)

        parsed_file = self._read_file(file)

        self._parse_properties(parsed_file, predicates)
        self._parse_levels(parsed_file, predicates)
        self._parse_counters(parsed_file, predicates)
        self._parse_techniques(
            parsed_file, ruleset, predicates)

        return predicates, ruleset

    def parse_input_file(self, file: str, predicates: dict[str, Vertex]) -> tuple[Vertex, dict[str, list[Vertex]]]:
        """Parses JSON with attack goal and attack factors

        Parameters
        ----------
        file : str
            file with attack goal and vectors
        predicates : dict[str, Vertex]
            all the possible vertices in the attack graph

        Returns
        -------
        tuple[Vertex, dict[str, list[Vertex]]]
            attack goal and attack factors
        """
        attack_factors: dict[str, list[Vertex]] = defaultdict(list)

        parsed_file = self._read_file(file)

        attack_goal = self._parse_attack_goal(parsed_file, predicates)
        self._parse_attack_factors(
            parsed_file, attack_factors, predicates)

        return attack_goal, attack_factors

    @staticmethod
    def _read_file(file: str) -> dict:
        with open(file, "r") as f:
            return json.loads(f.read())

    @staticmethod
    def _parse_properties(parsed_file: dict, predicates: dict[str, Vertex]) -> None:
        for k, v in parsed_file["PROPERTY"].items():
            predicates.update({k: Property(k, v)})

    @staticmethod
    def _parse_levels(parsed_file: dict, predicates: dict[str, Vertex]) -> None:
        for k, v in parsed_file["LEVEL"].items():
            predicates.update({k: Level(k, v)})

    @staticmethod
    def _parse_counters(parsed_file: dict, predicates: dict[str, Vertex]) -> None:
        for k, v in parsed_file["COUNTERMEASURE"].items():
            predicates.update({k: Countermeasure(k, v)})

    @staticmethod
    def _parse_techniques(parsed_file: dict, ruleset: dict[str, list[Technique]],
                          predicates: dict[str, Vertex]) -> None:
        for k, v in parsed_file["TECHNIQUE"].items():
            for technique in v:
                # TODO: bude se predelavat konstruktor technik
                ruleset[k].append(Technique(k, technique, predicates))

    @staticmethod
    def _parse_attack_goal(parsed_file: dict, predicates: dict[str, Vertex]) -> Vertex:
        """Sets attack goal from the input file

        Parameters
        ----------
        parsed_file : dict
            file contents converted to dictionary
        predicates : dict[str, Vertex]
            all the possible vertices in the attack graph

        Returns
        -------
        Vertex
            attack goal

        Raises
        ------
        Exception
            the attack goal is not found and the application can not run without it
        """
        attack_goal: Optional[Vertex] = None
        for k, v in parsed_file["attackGoal"].items():
            attack_goal = deepcopy(predicates[k])
            attack_goal.set_attributes(v)
        if attack_goal is None:
            raise Exception("attack goal is None")
        return attack_goal

    @staticmethod
    def _parse_attack_factors(parsed_file: dict, attack_factors: dict[str, list['Vertex']],
                              predicates: dict[str, Vertex]) -> None:
        """Sets attack factors from the input file

        Parameters
        ----------
        parsed_file : dict
            file contents converted to dictionary
        attack_factors : dict[str, list['Vertex']]
            dictionary to store the attack factors
        predicates : dict[str, Vertex]
            all the possible vertices in the attack graph
        """
        for k, v in parsed_file["vertices"].items():
            for attack_factor in v:
                predicate = deepcopy(predicates[k])
                predicate.set_attributes(attack_factor)
                attack_factors[k].append(predicate)
