from typing import Optional


def protocol_from_decimal(decimal: str | int) -> Optional[str]:
    """
    Return protocol name based on decimal value.
    See https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml

    @param decimal: str | int
        Decimal parameter value
    @return: protocol: Optional[str]
        Protocol name if found, else None
    """
    protocol_numbers = {
        "0": {
            "Keyword": "hopopt",
            "Protocol": "IPv6 Hop-by-Hop Option",
            "IPv6 Extension Header": "Y",
            "Reference": "[RFC8200]"
        },
        "1": {
            "Keyword": "icmp",
            "Protocol": "Internet Control Message",
            "IPv6 Extension Header": "",
            "Reference": "[RFC792]"
        },
        "2": {
            "Keyword": "igmp",
            "Protocol": "Internet Group Management",
            "IPv6 Extension Header": "",
            "Reference": "[RFC1112]"
        },
        "3": {
            "Keyword": "ggp",
            "Protocol": "Gateway-to-Gateway",
            "IPv6 Extension Header": "",
            "Reference": "[RFC823]"
        },
        "4": {
            "Keyword": "ipv4",
            "Protocol": "IPv4 encapsulation",
            "IPv6 Extension Header": "",
            "Reference": "[RFC2003]"
        },
        "5": {
            "Keyword": "st",
            "Protocol": "Stream",
            "IPv6 Extension Header": "",
            "Reference": "[RFC1190][RFC1819]"
        },
        "6": {
            "Keyword": "tcp",
            "Protocol": "Transmission Control",
            "IPv6 Extension Header": "",
            "Reference": "[RFC9293]"
        },
        "7": {
            "Keyword": "cbt",
            "Protocol": "CBT",
            "IPv6 Extension Header": "",
            "Reference": "[Tony_Ballardie]"
        },
        "8": {
            "Keyword": "egp",
            "Protocol": "Exterior Gateway Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[RFC888][David_Mills]"
        },
        "9": {
            "Keyword": "igp",
            "Protocol": "any private interior gateway             \n(used by Cisco for their IGRP)",
            "IPv6 Extension Header": "",
            "Reference": "[Internet_Assigned_Numbers_Authority]"
        },
        "10": {
            "Keyword": "bbn-rcc-mon",
            "Protocol": "BBN RCC Monitoring",
            "IPv6 Extension Header": "",
            "Reference": "[Steve_Chipman]"
        },
        "11": {
            "Keyword": "nvp-ii",
            "Protocol": "Network Voice Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[RFC741][Steve_Casner]"
        },
        "12": {
            "Keyword": "pup",
            "Protocol": "PUP",
            "IPv6 Extension Header": "",
            "Reference": "[Boggs, D., J. Shoch, E. Taft, and R. Metcalfe, \"PUP: An\nInternetwork Architecture\", XEROX Palo Alto Research Center,\nCSL-79-10, July 1979; also in IEEE Transactions on\nCommunication, Volume COM-28, Number 4, April 1980.][[XEROX]]"
        },
        "13": {
            "Keyword": "argus (deprecated)",
            "Protocol": "ARGUS",
            "IPv6 Extension Header": "",
            "Reference": "[Robert_W_Scheifler]"
        },
        "14": {
            "Keyword": "emcon",
            "Protocol": "EMCON",
            "IPv6 Extension Header": "",
            "Reference": "[Bich_Nguyen]"
        },
        "15": {
            "Keyword": "xnet",
            "Protocol": "Cross Net Debugger",
            "IPv6 Extension Header": "",
            "Reference": "[Haverty, J., \"XNET Formats for Internet Protocol Version 4\",\nIEN 158, October 1980.][Jack_Haverty]"
        },
        "16": {
            "Keyword": "chaos",
            "Protocol": "Chaos",
            "IPv6 Extension Header": "",
            "Reference": "[J_Noel_Chiappa]"
        },
        "17": {
            "Keyword": "udp",
            "Protocol": "User Datagram",
            "IPv6 Extension Header": "",
            "Reference": "[RFC768][Jon_Postel]"
        },
        "18": {
            "Keyword": "mux",
            "Protocol": "Multiplexing",
            "IPv6 Extension Header": "",
            "Reference": "[Cohen, D. and J. Postel, \"Multiplexing Protocol\", IEN 90,\nUSC/Information Sciences Institute, May 1979.][Jon_Postel]"
        },
        "19": {
            "Keyword": "dcn-meas",
            "Protocol": "DCN Measurement Subsystems",
            "IPv6 Extension Header": "",
            "Reference": "[David_Mills]"
        },
        "20": {
            "Keyword": "hmp",
            "Protocol": "Host Monitoring",
            "IPv6 Extension Header": "",
            "Reference": "[RFC869][Bob_Hinden]"
        },
        "21": {
            "Keyword": "prm",
            "Protocol": "Packet Radio Measurement",
            "IPv6 Extension Header": "",
            "Reference": "[Zaw_Sing_Su]"
        },
        "22": {
            "Keyword": "xns-idp",
            "Protocol": "XEROX NS IDP",
            "IPv6 Extension Header": "",
            "Reference": "[\"The Ethernet, A Local Area Network: Data Link Layer and\nPhysical Layer Specification\", AA-K759B-TK, Digital\nEquipment Corporation, Maynard, MA.  Also as: \"The\nEthernet - A Local Area Network\", Version 1.0, Digital\nEquipment Corporation, Intel Corporation, Xerox\nCorporation, September 1980.  And: \"The Ethernet, A Local\nArea Network: Data Link Layer and Physical Layer\nSpecifications\", Digital, Intel and Xerox, November 1982.\nAnd: XEROX, \"The Ethernet, A Local Area Network: Data Link\nLayer and Physical Layer Specification\", X3T51/80-50,\nXerox Corporation, Stamford, CT., October 1980.][[XEROX]]"
        },
        "23": {
            "Keyword": "trunk-1",
            "Protocol": "Trunk-1",
            "IPv6 Extension Header": "",
            "Reference": "[Barry_Boehm]"
        },
        "24": {
            "Keyword": "trunk-2",
            "Protocol": "Trunk-2",
            "IPv6 Extension Header": "",
            "Reference": "[Barry_Boehm]"
        },
        "25": {
            "Keyword": "leaf-1",
            "Protocol": "Leaf-1",
            "IPv6 Extension Header": "",
            "Reference": "[Barry_Boehm]"
        },
        "26": {
            "Keyword": "leaf-2",
            "Protocol": "Leaf-2",
            "IPv6 Extension Header": "",
            "Reference": "[Barry_Boehm]"
        },
        "27": {
            "Keyword": "rdp",
            "Protocol": "Reliable Data Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[RFC908][Bob_Hinden]"
        },
        "28": {
            "Keyword": "irtp",
            "Protocol": "Internet Reliable Transaction",
            "IPv6 Extension Header": "",
            "Reference": "[RFC938][Trudy_Miller]"
        },
        "29": {
            "Keyword": "iso-tp4",
            "Protocol": "ISO Transport Protocol Class 4",
            "IPv6 Extension Header": "",
            "Reference": "[RFC905][Robert_Cole]"
        },
        "30": {
            "Keyword": "netblt",
            "Protocol": "Bulk Data Transfer Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[RFC969][David_Clark]"
        },
        "31": {
            "Keyword": "mfe-nsp",
            "Protocol": "MFE Network Services Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Shuttleworth, B., \"A Documentary of MFENet, a National\nComputer Network\", UCRL-52317, Lawrence Livermore Labs,\nLivermore, California, June 1977.][Barry_Howard]"
        },
        "32": {
            "Keyword": "merit-inp",
            "Protocol": "MERIT Internodal Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Hans_Werner_Braun]"
        },
        "33": {
            "Keyword": "dccp",
            "Protocol": "Datagram Congestion Control Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[RFC4340]"
        },
        "34": {
            "Keyword": "3pc",
            "Protocol": "Third Party Connect Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Stuart_A_Friedberg]"
        },
        "35": {
            "Keyword": "idrp",
            "Protocol": "Inter-Domain Policy Routing Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Martha_Steenstrup]"
        },
        "36": {
            "Keyword": "xtp",
            "Protocol": "XTP",
            "IPv6 Extension Header": "",
            "Reference": "[Greg_Chesson]"
        },
        "37": {
            "Keyword": "ddp",
            "Protocol": "Datagram Delivery Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Wesley_Craig]"
        },
        "38": {
            "Keyword": "idpr-cmpt",
            "Protocol": "IDPR Control Message Transport Proto",
            "IPv6 Extension Header": "",
            "Reference": "[Martha_Steenstrup]"
        },
        "39": {
            "Keyword": "tp++",
            "Protocol": "TP++ Transport Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Dirk_Fromhein]"
        },
        "40": {
            "Keyword": "il",
            "Protocol": "IL Transport Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Dave_Presotto]"
        },
        "41": {
            "Keyword": "ipv6",
            "Protocol": "IPv6 encapsulation",
            "IPv6 Extension Header": "",
            "Reference": "[RFC2473]"
        },
        "42": {
            "Keyword": "sdrp",
            "Protocol": "Source Demand Routing Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Deborah_Estrin]"
        },
        "43": {
            "Keyword": "ipv6-Route",
            "Protocol": "Routing Header for IPv6",
            "IPv6 Extension Header": "Y",
            "Reference": "[Steve_Deering]"
        },
        "44": {
            "Keyword": "ipv6-Frag",
            "Protocol": "Fragment Header for IPv6",
            "IPv6 Extension Header": "Y",
            "Reference": "[Steve_Deering]"
        },
        "45": {
            "Keyword": "idrp",
            "Protocol": "Inter-Domain Routing Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Sue_Hares]"
        },
        "46": {
            "Keyword": "rsvp",
            "Protocol": "Reservation Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[RFC2205][RFC3209][Bob_Braden]"
        },
        "47": {
            "Keyword": "gre",
            "Protocol": "Generic Routing Encapsulation",
            "IPv6 Extension Header": "",
            "Reference": "[RFC2784][Tony_Li]"
        },
        "48": {
            "Keyword": "dsr",
            "Protocol": "Dynamic Source Routing Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[RFC4728]"
        },
        "49": {
            "Keyword": "bna",
            "Protocol": "BNA",
            "IPv6 Extension Header": "",
            "Reference": "[Gary Salamon]"
        },
        "50": {
            "Keyword": "esp",
            "Protocol": "Encap Security Payload",
            "IPv6 Extension Header": "Y",
            "Reference": "[RFC4303]"
        },
        "51": {
            "Keyword": "ah",
            "Protocol": "Authentication Header",
            "IPv6 Extension Header": "Y",
            "Reference": "[RFC4302]"
        },
        "52": {
            "Keyword": "i-nlsp",
            "Protocol": "Integrated Net Layer Security  TUBA",
            "IPv6 Extension Header": "",
            "Reference": "[K_Robert_Glenn]"
        },
        "53": {
            "Keyword": "swipe (deprecated)",
            "Protocol": "IP with Encryption",
            "IPv6 Extension Header": "",
            "Reference": "[John_Ioannidis]"
        },
        "54": {
            "Keyword": "narp",
            "Protocol": "NBMA Address Resolution Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[RFC1735]"
        },
        "55": {
            "Keyword": "min-ipv4",
            "Protocol": "Minimal IPv4 Encapsulation",
            "IPv6 Extension Header": "",
            "Reference": "[RFC2004][Charlie_Perkins]"
        },
        "56": {
            "Keyword": "tlsp",
            "Protocol": "Transport Layer Security Protocol        \nusing Kryptonet key management",
            "IPv6 Extension Header": "",
            "Reference": "[Christer_Oberg]"
        },
        "57": {
            "Keyword": "skip",
            "Protocol": "SKIP",
            "IPv6 Extension Header": "",
            "Reference": "[Tom_Markson]"
        },
        "58": {
            "Keyword": "ipv6-icmp",
            "Protocol": "ICMP for IPv6",
            "IPv6 Extension Header": "",
            "Reference": "[RFC8200]"
        },
        "59": {
            "Keyword": "ipv6-nonxt",
            "Protocol": "No Next Header for IPv6",
            "IPv6 Extension Header": "",
            "Reference": "[RFC8200]"
        },
        "60": {
            "Keyword": "ipv6-opts",
            "Protocol": "Destination Options for IPv6",
            "IPv6 Extension Header": "Y",
            "Reference": "[RFC8200]"
        },
        "61": {
            "Keyword": "",
            "Protocol": "any host internal protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Internet_Assigned_Numbers_Authority]"
        },
        "62": {
            "Keyword": "cftp",
            "Protocol": "CFTP",
            "IPv6 Extension Header": "",
            "Reference": "[Forsdick, H., \"CFTP\", Network Message, Bolt Beranek and\nNewman, January 1982.][Harry_Forsdick]"
        },
        "63": {
            "Keyword": "",
            "Protocol": "any local network",
            "IPv6 Extension Header": "",
            "Reference": "[Internet_Assigned_Numbers_Authority]"
        },
        "64": {
            "Keyword": "sat-expak",
            "Protocol": "SATNET and Backroom EXPAK",
            "IPv6 Extension Header": "",
            "Reference": "[Steven_Blumenthal]"
        },
        "65": {
            "Keyword": "kryptolan",
            "Protocol": "Kryptolan",
            "IPv6 Extension Header": "",
            "Reference": "[Paul Liu]"
        },
        "66": {
            "Keyword": "rvd",
            "Protocol": "MIT Remote Virtual Disk Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Michael_Greenwald]"
        },
        "67": {
            "Keyword": "ippc",
            "Protocol": "Internet Pluribus Packet Core",
            "IPv6 Extension Header": "",
            "Reference": "[Steven_Blumenthal]"
        },
        "68": {
            "Keyword": "",
            "Protocol": "any distributed file system",
            "IPv6 Extension Header": "",
            "Reference": "[Internet_Assigned_Numbers_Authority]"
        },
        "69": {
            "Keyword": "sat-mon",
            "Protocol": "SATNET Monitoring",
            "IPv6 Extension Header": "",
            "Reference": "[Steven_Blumenthal]"
        },
        "70": {
            "Keyword": "visa",
            "Protocol": "VISA Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Gene_Tsudik]"
        },
        "71": {
            "Keyword": "ipcv",
            "Protocol": "Internet Packet Core Utility",
            "IPv6 Extension Header": "",
            "Reference": "[Steven_Blumenthal]"
        },
        "72": {
            "Keyword": "cpnx",
            "Protocol": "Computer Protocol Network Executive",
            "IPv6 Extension Header": "",
            "Reference": "[David Mittnacht]"
        },
        "73": {
            "Keyword": "cphb",
            "Protocol": "Computer Protocol Heart Beat",
            "IPv6 Extension Header": "",
            "Reference": "[David Mittnacht]"
        },
        "74": {
            "Keyword": "wsn",
            "Protocol": "Wang Span Network",
            "IPv6 Extension Header": "",
            "Reference": "[Victor Dafoulas]"
        },
        "75": {
            "Keyword": "pvp",
            "Protocol": "Packet Video Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Steve_Casner]"
        },
        "76": {
            "Keyword": "br-sat-mon",
            "Protocol": "Backroom SATNET Monitoring",
            "IPv6 Extension Header": "",
            "Reference": "[Steven_Blumenthal]"
        },
        "77": {
            "Keyword": "sun-nd",
            "Protocol": "SUN ND PROTOCOL-Temporary",
            "IPv6 Extension Header": "",
            "Reference": "[William_Melohn]"
        },
        "78": {
            "Keyword": "wb-mon",
            "Protocol": "WIDEBAND Monitoring",
            "IPv6 Extension Header": "",
            "Reference": "[Steven_Blumenthal]"
        },
        "79": {
            "Keyword": "wb-expak",
            "Protocol": "WIDEBAND EXPAK",
            "IPv6 Extension Header": "",
            "Reference": "[Steven_Blumenthal]"
        },
        "80": {
            "Keyword": "iso-ip",
            "Protocol": "ISO Internet Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Marshall_T_Rose]"
        },
        "81": {
            "Keyword": "vmtp",
            "Protocol": "VMTP",
            "IPv6 Extension Header": "",
            "Reference": "[Dave_Cheriton]"
        },
        "82": {
            "Keyword": "secure-vmtp",
            "Protocol": "SECURE-VMTP",
            "IPv6 Extension Header": "",
            "Reference": "[Dave_Cheriton]"
        },
        "83": {
            "Keyword": "vines",
            "Protocol": "VINES",
            "IPv6 Extension Header": "",
            "Reference": "[Brian Horn]"
        },
        "84": {
            "Keyword": "iptm",
            "Protocol": "Internet Protocol Traffic Manager",
            "IPv6 Extension Header": "",
            "Reference": "[Jim_Stevens][1]"
        },
        "85": {
            "Keyword": "nsfnet-igp",
            "Protocol": "NSFNET-IGP",
            "IPv6 Extension Header": "",
            "Reference": "[Hans_Werner_Braun]"
        },
        "86": {
            "Keyword": "dgp",
            "Protocol": "Dissimilar Gateway Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[M/A-COM Government Systems, \"Dissimilar Gateway Protocol\nSpecification, Draft Version\", Contract no. CS901145,\nNovember 16, 1987.][Mike_Little]"
        },
        "87": {
            "Keyword": "tcf",
            "Protocol": "TCF",
            "IPv6 Extension Header": "",
            "Reference": "[Guillermo_A_Loyola]"
        },
        "88": {
            "Keyword": "eigrp",
            "Protocol": "EIGRP",
            "IPv6 Extension Header": "",
            "Reference": "[RFC7868]"
        },
        "89": {
            "Keyword": "ospfigp",
            "Protocol": "OSPFIGP",
            "IPv6 Extension Header": "",
            "Reference": "[RFC1583][RFC2328][RFC5340][John_Moy]"
        },
        "90": {
            "Keyword": "sprite-rpc",
            "Protocol": "Sprite RPC Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Welch, B., \"The Sprite Remote Procedure Call System\",\nTechnical Report, UCB/Computer Science Dept., 86/302,\nUniversity of California at Berkeley, June 1986.][Bruce Willins]"
        },
        "91": {
            "Keyword": "larp",
            "Protocol": "Locus Address Resolution Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Brian Horn]"
        },
        "92": {
            "Keyword": "mtp",
            "Protocol": "Multicast Transport Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Susie_Armstrong]"
        },
        "93": {
            "Keyword": "ax.25",
            "Protocol": "AX.25 Frames",
            "IPv6 Extension Header": "",
            "Reference": "[Brian_Kantor]"
        },
        "94": {
            "Keyword": "ipip",
            "Protocol": "IP-within-IP Encapsulation Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[John_Ioannidis]"
        },
        "95": {
            "Keyword": "micp (deprecated)",
            "Protocol": "Mobile Internetworking Control Pro.",
            "IPv6 Extension Header": "",
            "Reference": "[John_Ioannidis]"
        },
        "96": {
            "Keyword": "ssc-sp",
            "Protocol": "Semaphore Communications Sec. Pro.",
            "IPv6 Extension Header": "",
            "Reference": "[Howard_Hart]"
        },
        "97": {
            "Keyword": "etherip",
            "Protocol": "Ethernet-within-IP Encapsulation",
            "IPv6 Extension Header": "",
            "Reference": "[RFC3378]"
        },
        "98": {
            "Keyword": "encap",
            "Protocol": "Encapsulation Header",
            "IPv6 Extension Header": "",
            "Reference": "[RFC1241][Robert_Woodburn]"
        },
        "99": {
            "Keyword": "",
            "Protocol": "any private encryption scheme",
            "IPv6 Extension Header": "",
            "Reference": "[Internet_Assigned_Numbers_Authority]"
        },
        "100": {
            "Keyword": "gmtp",
            "Protocol": "GMTP",
            "IPv6 Extension Header": "",
            "Reference": "[[RXB5]]"
        },
        "101": {
            "Keyword": "ifmp",
            "Protocol": "Ipsilon Flow Management Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Bob_Hinden][November 1995, 1997.]"
        },
        "102": {
            "Keyword": "pnni",
            "Protocol": "PNNI over IP",
            "IPv6 Extension Header": "",
            "Reference": "[Ross_Callon]"
        },
        "103": {
            "Keyword": "pim",
            "Protocol": "Protocol Independent Multicast",
            "IPv6 Extension Header": "",
            "Reference": "[RFC7761][Dino_Farinacci]"
        },
        "104": {
            "Keyword": "aris",
            "Protocol": "ARIS",
            "IPv6 Extension Header": "",
            "Reference": "[Nancy_Feldman]"
        },
        "105": {
            "Keyword": "scps",
            "Protocol": "SCPS",
            "IPv6 Extension Header": "",
            "Reference": "[Robert_Durst]"
        },
        "106": {
            "Keyword": "qnx",
            "Protocol": "QNX",
            "IPv6 Extension Header": "",
            "Reference": "[Michael_Hunter]"
        },
        "107": {
            "Keyword": "a/n",
            "Protocol": "Active Networks",
            "IPv6 Extension Header": "",
            "Reference": "[Bob_Braden]"
        },
        "108": {
            "Keyword": "ipcomp",
            "Protocol": "IP Payload Compression Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[RFC2393]"
        },
        "109": {
            "Keyword": "snp",
            "Protocol": "Sitara Networks Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Manickam_R_Sridhar]"
        },
        "110": {
            "Keyword": "compaq-peer",
            "Protocol": "Compaq Peer Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Victor_Volpe]"
        },
        "111": {
            "Keyword": "ipx-in-ip",
            "Protocol": "IPX in IP",
            "IPv6 Extension Header": "",
            "Reference": "[CJ_Lee]"
        },
        "112": {
            "Keyword": "vrrp",
            "Protocol": "Virtual Router Redundancy Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[RFC5798]"
        },
        "113": {
            "Keyword": "pgm",
            "Protocol": "PGM Reliable Transport Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Tony_Speakman]"
        },
        "114": {
            "Keyword": "",
            "Protocol": "any 0-hop protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Internet_Assigned_Numbers_Authority]"
        },
        "115": {
            "Keyword": "l2tp",
            "Protocol": "Layer Two Tunneling Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[RFC3931][Bernard_Aboba]"
        },
        "116": {
            "Keyword": "ddx",
            "Protocol": "D-II Data Exchange (DDX)",
            "IPv6 Extension Header": "",
            "Reference": "[John_Worley]"
        },
        "117": {
            "Keyword": "iatp",
            "Protocol": "Interactive Agent Transfer Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[John_Murphy]"
        },
        "118": {
            "Keyword": "stp",
            "Protocol": "Schedule Transfer Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Jean_Michel_Pittet]"
        },
        "119": {
            "Keyword": "srp",
            "Protocol": "SpectraLink Radio Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Mark_Hamilton]"
        },
        "120": {
            "Keyword": "uti",
            "Protocol": "UTI",
            "IPv6 Extension Header": "",
            "Reference": "[Peter_Lothberg]"
        },
        "121": {
            "Keyword": "smp",
            "Protocol": "Simple Message Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Leif_Ekblad]"
        },
        "122": {
            "Keyword": "sm (deprecated)",
            "Protocol": "Simple Multicast Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Jon_Crowcroft][draft-perlman-simple-multicast]"
        },
        "123": {
            "Keyword": "ptp",
            "Protocol": "Performance Transparency Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Michael_Welzl]"
        },
        "124": {
            "Keyword": "isis over ipv4",
            "Protocol": "",
            "IPv6 Extension Header": "",
            "Reference": "[Tony_Przygienda]"
        },
        "125": {
            "Keyword": "fire",
            "Protocol": "",
            "IPv6 Extension Header": "",
            "Reference": "[Criag_Partridge]"
        },
        "126": {
            "Keyword": "crtp",
            "Protocol": "Combat Radio Transport Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Robert_Sautter]"
        },
        "127": {
            "Keyword": "crudp",
            "Protocol": "Combat Radio User Datagram",
            "IPv6 Extension Header": "",
            "Reference": "[Robert_Sautter]"
        },
        "128": {
            "Keyword": "sscopmce",
            "Protocol": "",
            "IPv6 Extension Header": "",
            "Reference": "[Kurt_Waber]"
        },
        "129": {
            "Keyword": "iplt",
            "Protocol": "",
            "IPv6 Extension Header": "",
            "Reference": "[[Hollbach]]"
        },
        "130": {
            "Keyword": "sps",
            "Protocol": "Secure Packet Shield",
            "IPv6 Extension Header": "",
            "Reference": "[Bill_McIntosh]"
        },
        "131": {
            "Keyword": "pipe",
            "Protocol": "Private IP Encapsulation within IP",
            "IPv6 Extension Header": "",
            "Reference": "[Bernhard_Petri]"
        },
        "132": {
            "Keyword": "sctp",
            "Protocol": "Stream Control Transmission Protocol",
            "IPv6 Extension Header": "",
            "Reference": "[Randall_R_Stewart]"
        },
        "133": {
            "Keyword": "fc",
            "Protocol": "Fibre Channel",
            "IPv6 Extension Header": "",
            "Reference": "[Murali_Rajagopal][RFC6172]"
        },
        "134": {
            "Keyword": "rsvp-e2e-ignore",
            "Protocol": "",
            "IPv6 Extension Header": "",
            "Reference": "[RFC3175]"
        },
        "135": {
            "Keyword": "mobility header",
            "Protocol": "",
            "IPv6 Extension Header": "Y",
            "Reference": "[RFC6275]"
        },
        "136": {
            "Keyword": "udplite",
            "Protocol": "",
            "IPv6 Extension Header": "",
            "Reference": "[RFC3828]"
        },
        "137": {
            "Keyword": "mpls-in-ip",
            "Protocol": "",
            "IPv6 Extension Header": "",
            "Reference": "[RFC4023]"
        },
        "138": {
            "Keyword": "manet",
            "Protocol": "MANET Protocols",
            "IPv6 Extension Header": "",
            "Reference": "[RFC5498]"
        },
        "139": {
            "Keyword": "hip",
            "Protocol": "Host Identity Protocol",
            "IPv6 Extension Header": "Y",
            "Reference": "[RFC7401]"
        },
        "140": {
            "Keyword": "shim6",
            "Protocol": "Shim6 Protocol",
            "IPv6 Extension Header": "Y",
            "Reference": "[RFC5533]"
        },
        "141": {
            "Keyword": "wesp",
            "Protocol": "Wrapped Encapsulating Security Payload",
            "IPv6 Extension Header": "",
            "Reference": "[RFC5840]"
        },
        "142": {
            "Keyword": "rohc",
            "Protocol": "Robust Header Compression",
            "IPv6 Extension Header": "",
            "Reference": "[RFC5858]"
        },
        "143": {
            "Keyword": "ethernet",
            "Protocol": "Ethernet",
            "IPv6 Extension Header": "",
            "Reference": "[RFC8986]"
        },
        "144": {
            "Keyword": "aggfrag",
            "Protocol": "AGGFRAG encapsulation payload for ESP",
            "IPv6 Extension Header": "",
            "Reference": "[RFC9347]"
        },
        "145": {
            "Keyword": "nsh",
            "Protocol": "Network Service Header",
            "IPv6 Extension Header": "N",
            "Reference": "[RFC9491]"
        },
        "253": {
            "Keyword": "",
            "Protocol": "Use for experimentation and testing",
            "IPv6 Extension Header": "Y",
            "Reference": "[RFC3692]"
        },
        "254": {
            "Keyword": "",
            "Protocol": "Use for experimentation and testing",
            "IPv6 Extension Header": "Y",
            "Reference": "[RFC3692]"
        },
        "255": {
            "Keyword": "reserved",
            "Protocol": "",
            "IPv6 Extension Header": "",
            "Reference": "[Internet_Assigned_Numbers_Authority]"
        },
        "146-252": {
            "Keyword": "",
            "Protocol": "unassigned",
            "IPv6 Extension Header": "",
            "Reference": "[Internet_Assigned_Numbers_Authority]"
        }
    }
    if str(decimal) in protocol_numbers:
        return protocol_numbers[str(decimal)]["Keyword"]
    return None
