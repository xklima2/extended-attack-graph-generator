from src.input_generating.utils import cartesian_product


class Countermeasures:
    """
    Class used to generate countermeasures. Needed values are stored in it when processing logs
    and countermeasures are reated from them later.

    Attributes:
        template_dict (dict[str, list[str]]): Dictionary stroing the template defined in ruleset
                                              for every countermeasure
        attributes (dict[str, set[str | int]]): Dictionary storing the actual values from logs
                                                for parameters defined int the ruleset that
    """
    def __init__(self, ruleset_countermeasures: dict[str, list[str]]) -> None:
        """
        Generates countermeasure template dict that is later combined with parameters from log files to create
        countermeasures in generated input file.
        Also creates dict with all the attribute names in countermeasures that is used to find all the attribute values
        in the log files.
        @param ruleset_countermeasures: dict[str, list[str]]
            Dictionary representing coutermeasures definition in the ruleset
        """
        self.template_dict: dict[str, list[str]] = {}
        attribute_names: set[str] = set()

        for name, attributes in ruleset_countermeasures.items():
            attribute_names.update(attributes)
            self.template_dict[name] = ["false" if attr == "bool" else attr.upper() for attr in attributes]

        if "bool" in ruleset_countermeasures:
            attribute_names.remove("bool")
        attribute_names = {attr.upper() for attr in attribute_names}

        self.attributes: dict[str, set[str | int]] = {attr: set() for attr in attribute_names}

    def create_countermeasures(self) -> dict[str, list[list[str | int]]]:
        """
        Creates dictionary containing countermeasures with all possible filled predicates values.
        If there is no matching value for parameter, '_' is inserted

        @return: countermeasures_filled_dict: dict[str, list[list[str | int]]]
            Dict with countermeasures names as keys and list of filled predicates as values
        """
        countermeasures_filled_dict:  dict[str, list[list[str | int]]] = {}

        for countermeasure, attributes in self.template_dict.items():
            if len(attributes) == 1:
                assert attributes == ["false"]
                countermeasures_filled_dict[countermeasure] = [["false"]]
                continue

            possible_values: list[set[str | int]] = []
            for attribute in attributes:
                if attribute != "false":
                    possible_values.append(self.attributes[attribute])
            possible_values = [s if len(s) != 0 else set("_") for s in possible_values]

            bool_index = attributes.index("false")
            combinations = []
            for combination in cartesian_product(possible_values):
                combination.insert(bool_index, "false")
                combinations.append(combination)

            countermeasures_filled_dict[countermeasure] = combinations

        return countermeasures_filled_dict
