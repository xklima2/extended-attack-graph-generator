from json import loads
from typing import Generator, Optional

from src.input_generating.utils import transform_ipflow


def yield_json(file_path: str, start: Optional[int], end: Optional[int], is_syslog: bool) \
        -> Generator[dict[str, str | int], None, None]:
    """
    Iterates over file with logs from Syslog/IPFlow and yields them in a desirable format. Also can yield only logs
    with timestamp within given interval
    @param file_path: str
        Path to file with Syslog/IPFlow logs
    @param start: Optional[int]
        Start timestamp to filter logs. Defines the upper bound of the interval if is not None
    @param end: Optional[int]
        End timestamp to filter logs. Defines the lower bound of the interval if is not None
    @param is_syslog: bool
        True if yuelding from Syslog, False if from IPFlow
    @return: log: dict[str, str | int]
        Single log from Syslog/IPFlow with uppercase keys and values and timestamp within given interval
    """
    with open(file_path) as file:
        for line in file:
            log = {k.upper(): v.lower() if isinstance(v, str) else v for k, v in loads(line).items()}

            if start is not None and log["TIMESTAMP"] // 1000 < start:
                continue
            if end is not None and log["TIMESTAMP"] // 1000 > end:
                continue

            if not is_syslog:
                transform_ipflow(log)

            yield log
