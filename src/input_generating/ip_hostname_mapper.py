from typing import Optional


def get_ip_map() -> dict[str,  str]:
    """
    Returns a dictionary containig mapping of IP addresses to their corresponding hostnames

    @return: ip_map: dict[str, int | str]
        Mapping of IP addresses to their corresponding hostnames
    """
    return {
        # VPN-BT
        "10.1.5.2": "tablet-bt1",
        "10.2.5.2": "tablet-bt2",
        "10.3.5.2": "tablet-bt3",
        "10.4.5.2": "tablet-bt4",
        "10.5.5.2": "tablet-bt5",
        "10.6.5.2": "tablet-bt6",

        # DMZ-BT
        "9.66.11.12": "mail",
        "9.66.22.12": "mail",
        "9.66.33.12": "mail",
        "9.66.44.12": "mail",
        "9.66.55.12": "mail",
        "9.66.66.12": "mail",

        "9.66.11.13": "dns",
        "9.66.22.13": "dns",
        "9.66.33.13": "dns",
        "9.66.44.13": "dns",
        "9.66.55.13": "dns",
        "9.66.66.13": "dns",

        "9.66.11.14": "www.firechmel.ex",
        "9.66.22.14": "www.firechmel.ex",
        "9.66.33.14": "www.firechmel.ex",
        "9.66.44.14": "www.firechmel.ex",
        "9.66.55.14": "www.firechmel.ex",
        "9.66.66.14": "www.firechmel.ex",

        # SRV-BT
        "10.1.2.22": "dc",
        "10.2.2.22": "dc",
        "10.3.2.22": "dc",
        "10.4.2.22": "dc",
        "10.5.2.22": "dc",
        "10.6.2.22": "dc",

        "10.1.2.23": "files",
        "10.2.2.23": "files",
        "10.3.2.23": "files",
        "10.4.2.23": "files",
        "10.5.2.23": "files",
        "10.6.2.23": "files",

        "10.1.2.24": "backup",
        "10.2.2.24": "backup",
        "10.3.2.24": "backup",
        "10.4.2.24": "backup",
        "10.5.2.24": "backup",
        "10.6.2.24": "backup",

        "10.1.2.25": "menu",
        "10.2.2.25": "menu",
        "10.3.2.25": "menu",
        "10.4.2.25": "menu",
        "10.5.2.25": "menu",
        "10.6.2.25": "menu",

        "10.1.2.26": "db.chmel.ex",
        "10.2.2.26": "db.chmel.ex",
        "10.3.2.26": "db.chmel.ex",
        "10.4.2.26": "db.chmel.ex",
        "10.5.2.26": "db.chmel.ex",
        "10.6.2.26": "db.chmel.ex",

        "10.1.2.27": "ocs.chmel.ex",
        "10.2.2.27": "ocs.chmel.ex",
        "10.3.2.27": "ocs.chmel.ex",
        "10.4.2.27": "ocs.chmel.ex",
        "10.5.2.27": "ocs.chmel.ex",
        "10.6.2.27": "ocs.chmel.ex",

        "10.1.2.28": "ups.chmel.ex",
        "10.2.2.28": "ups.chmel.ex",
        "10.3.2.28": "ups.chmel.ex",
        "10.4.2.28": "ups.chmel.ex",
        "10.5.2.28": "ups.chmel.ex",
        "10.6.2.28": "ups.chmel.ex",

        "10.1.2.29": "monitoring",
        "10.2.2.29": "monitoring",
        "10.3.2.29": "monitoring",
        "10.4.2.29": "monitoring",
        "10.5.2.29": "monitoring",
        "10.6.2.29": "monitoring",

        # OPS-BT
        "10.1.3.32": "ops-desktop1",
        "10.2.3.32": "ops-desktop1",
        "10.3.3.32": "ops-desktop1",
        "10.4.3.32": "ops-desktop1",
        "10.5.3.32": "ops-desktop1",
        "10.6.3.32": "ops-desktop1",

        "10.1.3.33": "ops-desktop2",
        "10.2.3.33": "ops-desktop2",
        "10.3.3.33": "ops-desktop2",
        "10.4.3.33": "ops-desktop2",
        "10.5.3.33": "ops-desktop2",
        "10.6.3.33": "ops-desktop2",

        "10.1.3.34": "insider",
        "10.2.3.34": "insider",
        "10.3.3.34": "insider",
        "10.4.3.34": "insider",
        "10.5.3.34": "insider",
        "10.6.3.34": "insider",

        # USR-BT
        "10.1.4.42": "desktop1",
        "10.2.4.42": "desktop1",
        "10.3.4.42": "desktop1",
        "10.4.4.42": "desktop1",
        "10.5.4.42": "desktop1",
        "10.6.4.42": "desktop1",

        "10.1.4.43": "desktop2",
        "10.2.4.43": "desktop2",
        "10.3.4.43": "desktop2",
        "10.4.4.43": "desktop2",
        "10.5.4.43": "desktop2",
        "10.6.4.43": "desktop2",

        "10.1.4.44": "desktop3",
        "10.2.4.44": "desktop3",
        "10.3.4.44": "desktop3",
        "10.4.4.44": "desktop3",
        "10.5.4.44": "desktop3",
        "10.6.4.44": "desktop3",

        "10.1.4.45": "desktop4",
        "10.2.4.45": "desktop4",
        "10.3.4.45": "desktop4",
        "10.4.4.45": "desktop4",
        "10.5.4.45": "desktop4",
        "10.6.4.45": "desktop4",

        "10.1.4.46": "admin1",
        "10.2.4.46": "admin1",
        "10.3.4.46": "admin1",
        "10.4.4.46": "admin1",
        "10.5.4.46": "admin1",
        "10.6.4.46": "admin1",

        "10.1.4.47": "admin2",
        "10.2.4.47": "admin2",
        "10.3.4.47": "admin2",
        "10.4.4.47": "admin2",
        "10.5.4.47": "admin2",
        "10.6.4.47": "admin2",

        "10.1.4.48": "admin3",
        "10.2.4.48": "admin3",
        "10.3.4.48": "admin3",
        "10.4.4.48": "admin3",
        "10.5.4.48": "admin3",
        "10.6.4.48": "admin3",

        "10.1.4.49": "admin4",
        "10.2.4.49": "admin4",
        "10.3.4.49": "admin4",
        "10.4.4.49": "admin4",
        "10.5.4.49": "admin4",
        "10.6.4.49": "admin4",

        # MGMT
        "10.7.96.97": "central-conﬁg",
        "10.7.96.96": "central-scoring",

        "10.7.101.250": "scoring-blue1",
        "10.7.102.250": "scoring-blue2",
        "10.7.103.250": "scoring-blue3",
        "10.7.104.250": "scoring-blue4",
        "10.7.105.250": "scoring-blue5",
        "10.7.106.250": "scoring-blue6",

        # GLOBAL
        # "4.122.55.2": "global-web",
        # "4.122.55.3": "global-dns",
        # "4.122.55.4": "global-mail",
        # "4.122.55.5": "desktop1",
        # "4.122.55.6": "desktop2",
        # "4.122.55.7": "global-app",

        "4.122.55.21": "test1",
        "4.122.55.22": "test2",
        "4.122.55.23": "test3",
        "4.122.55.24": "test4",
        "4.122.55.25": "test5",
        "4.122.55.26": "test6",

        "4.122.55.250": "nagios-global"
    }


def ipfix_ip_to_hostname(ip: str) -> Optional[str]:
    """
    Return hostname based on ip address. Corrrespond to topology in auxiliary materials
    at https://zenodo.org/records/3746129

    @param ip: str | int
        String representing ip address from ipfix flow
    @return: hostname: Optional[str]
        Hostname if found, else None
    """
    ip_map = get_ip_map()
    if ip in ip_map:
        return ip_map[ip]

    return None
