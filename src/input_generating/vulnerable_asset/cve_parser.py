import datetime
import os
import urllib.request
import zipfile
import json


class Vulnerability:
    """
    Class for representing necessary information about vulnerability.
    Dictionary is used for storing cvssv2 and cvssv3 because of constant access.
    cpe_type contains values:
        'h' for hardware,
        'a' for application,
        'o' for operating system.
    """

    def __init__(self):
        self.description = ""
        self.cwe = ""
        self.cvssv2 = {}
        self.cvssv3 = {}
        self.cpe_type = set()
        self.cpe_configurations = []
        self.published_date = ""


def download_necessary_files():
    """
    Download CVE files.
    """

    if not os.path.exists('cve-data'):
        os.makedirs('cve-data')

    i = 2017
    # while i <= datetime.datetime.now().year:
    while i <= 2017:
        if not os.path.exists('cve-data/nvdcve-1.1-{0}.json'.format(i)):
            urllib.request.urlretrieve(
                'https://nvd.nist.gov/feeds/json/cve/1.1/nvdcve-1.1-{0}.json.zip'.format(i),
                'cve-data/nvdcve-1.1-{0}.zip'.format(i))
        with zipfile.ZipFile('cve-data/nvdcve-1.1-{0}.zip'.format(i), 'r') as cve_zip:
            cve_zip.extractall('cve-data')
        i += 1


def read_lines_with_no_information(name_of_file):
    """
    Reads blocks with unnecessary information.
    :param name_of_file: The name of file from which function will read.
    Block ends with line containing only "},".
    """

    line = name_of_file.readline()
    while ('},' not in line) or ('} , {' in line):
        line = name_of_file.readline()


def parse(name_of_file, specified_time):
    """
    This function parses content of JSON files from the NVD.

    :param name_of_file: Source file.
    :param specified_time: time in isoformat - files with timestamp after this time are processed
    :return: Dictionary containing parsed data
    """

    dict_of_vulnerabilities = {}
    with open(name_of_file, "r", encoding='UTF-8') as cve_file:
        data = json.load(cve_file)

    for item in data["CVE_Items"]:
        if not data['CVE_data_timestamp'] > specified_time:
            return dict_of_vulnerabilities

        if item['lastModifiedDate'] < specified_time:
            # skip this CVE
            continue

        cve_id = item['cve']['CVE_data_meta']['ID']
        dict_of_vulnerabilities[cve_id] = Vulnerability()

        # CVE has only one description even though in json there is a list
        dict_of_vulnerabilities[cve_id].description = \
            item['cve']['description']['description_data'][0]['value']

        # CVE has only one CWE (if any)
        if item['cve']['problemtype']['problemtype_data'][0]['description']:
            dict_of_vulnerabilities[cve_id].cwe = \
                item['cve']['problemtype']['problemtype_data'][0]['description'][0]['value']

        # source file contains in cvssv2 dictionary also values which we don't need
        if 'baseMetricV2' in item['impact']:
            cvssv2 = item['impact']['baseMetricV2']['cvssV2']
            dict_of_vulnerabilities[cve_id].cvssv2['av'] = cvssv2['accessVector']
            dict_of_vulnerabilities[cve_id].cvssv2['ac'] = cvssv2['accessComplexity']
            dict_of_vulnerabilities[cve_id].cvssv2['au'] = cvssv2['authentication']
            dict_of_vulnerabilities[cve_id].cvssv2['c'] = cvssv2['confidentialityImpact']
            dict_of_vulnerabilities[cve_id].cvssv2['i'] = cvssv2['integrityImpact']
            dict_of_vulnerabilities[cve_id].cvssv2['a'] = cvssv2['availabilityImpact']
            dict_of_vulnerabilities[cve_id].cvssv2['score'] = cvssv2['baseScore']

            dict_of_vulnerabilities[cve_id].cvssv2['exploitabilityScore'] = \
                item['impact']['baseMetricV2']['exploitabilityScore']
            dict_of_vulnerabilities[cve_id].cvssv2['impactScore'] = \
                item['impact']['baseMetricV2']['impactScore']
            dict_of_vulnerabilities[cve_id].cvssv2['severity'] = \
                item['impact']['baseMetricV2']['severity']
            dict_of_vulnerabilities[cve_id].cvssv2['userInteractionRequired'] = \
                item['impact']['baseMetricV2']['userInteractionRequired']

            # The flags does not relate to cvssv2 but were attached there
            dict_of_vulnerabilities[cve_id].cvssv2['obtainAllPrivilege'] = \
                item['impact']['baseMetricV2']['obtainAllPrivilege']
            dict_of_vulnerabilities[cve_id].cvssv2['obtainUserPrivilege'] = \
                item['impact']['baseMetricV2']['obtainUserPrivilege']
            dict_of_vulnerabilities[cve_id].cvssv2['obtainOtherPrivilege'] = \
                item['impact']['baseMetricV2']['obtainOtherPrivilege']

        # source file contains in cvssv3 dictionary also values which we don't need
        if 'baseMetricV3' in item['impact']:
            cvssv3 = item['impact']['baseMetricV3']['cvssV3']
            dict_of_vulnerabilities[cve_id].cvssv3['av'] = cvssv3['attackVector']
            dict_of_vulnerabilities[cve_id].cvssv3['ac'] = cvssv3['attackComplexity']
            dict_of_vulnerabilities[cve_id].cvssv3['pr'] = cvssv3['privilegesRequired']
            dict_of_vulnerabilities[cve_id].cvssv3['ui'] = cvssv3['userInteraction']
            dict_of_vulnerabilities[cve_id].cvssv3['s'] = cvssv3['scope']
            dict_of_vulnerabilities[cve_id].cvssv3['c'] = cvssv3['confidentialityImpact']
            dict_of_vulnerabilities[cve_id].cvssv3['i'] = cvssv3['integrityImpact']
            dict_of_vulnerabilities[cve_id].cvssv3['a'] = cvssv3['availabilityImpact']
            dict_of_vulnerabilities[cve_id].cvssv3['score'] = cvssv3['baseScore']
            dict_of_vulnerabilities[cve_id].cvssv3['baseSeverity'] = cvssv3['baseSeverity']

            dict_of_vulnerabilities[cve_id].cvssv3['exploitabilityScore'] = \
                item['impact']['baseMetricV3']['exploitabilityScore']
            dict_of_vulnerabilities[cve_id].cvssv3['impactScore'] = \
                item['impact']['baseMetricV3']['impactScore']

        dict_of_vulnerabilities[cve_id].published_date = item['publishedDate']
        dict_of_vulnerabilities[cve_id].cpe_configurations = item['configurations']['nodes']

        # parsing CPE information about part of system - application, operating system or hardware
        for cpe_item in item['configurations']['nodes']:
            if 'cpe_match' in cpe_item:
                for cpe in cpe_item['cpe_match']:
                    if cpe['vulnerable']:
                        dict_of_vulnerabilities[cve_id].cpe_type \
                            .add(cpe['cpe23Uri'])

            elif 'children' in cpe_item:
                for child in cpe_item['children']:
                    if 'cpe_match' in child:
                        for cpe in child['cpe_match']:
                            if cpe['vulnerable']:
                                dict_of_vulnerabilities[cve_id].cpe_type \
                                    .add(cpe['cpe23Uri'])

    return dict_of_vulnerabilities


def get_dict_of_vulnerability() -> dict[str, Vulnerability]:
    download_necessary_files()
    return parse('cve-data/nvdcve-1.1-{0}.json'.format(2017), "2017-01-01T00:00:00.000000")
