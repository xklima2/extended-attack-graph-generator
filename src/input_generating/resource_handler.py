from collections import defaultdict


class ResourceHandler:
    """
    Class to handle and pass resources. Mainly used for static attribute generating.

    Attributes:
        ruleset (dict[str, dict[str, list[str]]]): Dictionary representing given ruleset
        vertices_dict (dict): Dictionary to store  created vertices
        vulnerable_asset_attributes (set): Set to store vulnerable asset attributes
        hostnames (set): Set to store hostnames
        network_service_counter (dict): Dictionary to count hostname port pairs occurrences
    """
    def __init__(self, ruleset: dict[str, dict[str, list[str]]]):
        """
        Initializes the ResourceHandler with a ruleset.

        @param ruleset: dict[str, dict[str, list[str]]]
            Dictionary representing given ruleset
        """
        self.ruleset = ruleset
        self.vertices_dict: dict[str, list[list[str | int]]] = {}
        self.vulnerable_asset_attributes: set[tuple[str, str]] = set()
        self.hostnames: set[str] = set()
        self.network_service_counter: dict[str | int, dict[str | int, int]] = defaultdict(
            lambda: defaultdict(lambda: 0))
