import os.path
from collections import defaultdict
from json import load, loads, dumps

from src.input_generating.json_yielder import yield_json
from src.input_generating.utils import is_subset_upper, merge_dicts, filter_network_services
from src.input_generating.resource_handler import ResourceHandler
from src.arguments.arguments import Arguments
from src.input_generating.static_attributes import generate_static_attributes

from src.input_generating.countermeasures import Countermeasures


def generate_attack_goals(attack_goal_file_path: str) -> list[dict[str, list[str]]]:
    """
    Loads attack goal(s) for the generated input file

    @param attack_goal_file_path: str
        Path to file containing attack goal(s)
    @return: attack_goals: list[dict[str, list[str]]]
        Attack goals for the attack graph generator
    """
    with open(attack_goal_file_path) as file:
        attack_goals = [loads(line) for line in file]
    return attack_goals


def process_log_for_static_attributes(log: dict[str, str | int], resource_handler: ResourceHandler) -> None:
    """
    Peocesses single log for static attributes generating

    @param log: dict[str, str | int]
        Single Sysloog/IPFlow log
    @param resource_handler: ResourceHandler
    @return: None
    """
    if "HOSTNAME" in log.keys():
        resource_handler.hostnames.add(str(log["HOSTNAME"]))

    if is_subset_upper(set(log.keys()), {"HOSTNAME", "APP_NAME"}):
        assert isinstance(log["HOSTNAME"], str) and isinstance(log["APP_NAME"], str)
        resource_handler.vulnerable_asset_attributes.add((log["HOSTNAME"], log["APP_NAME"]))

    if "HOSTNAME" in log and "PORT" in log:
        resource_handler.network_service_counter[log["HOSTNAME"]][log["PORT"]] += 1


def process_log_for_countermeasures(log: dict[str, str | int], countermeasures: Countermeasures) -> None:
    """
    Peocesses single log for countermeaasures generating
     @param log: dict[str, str | int]
        Single Sysloog/IPFlow log
    @param countermeasures: Countermeasures
    @return: None
    """
    for key in log.keys():
        if key.upper() not in countermeasures.attributes.keys():
            continue
        countermeasures.attributes[key.upper()].add(log[key])


def match_predicates(arguments: Arguments,
                     predicates_ruleset: dict[str, list[str]],
                     countermeasures: Countermeasures,
                     resource_handler: ResourceHandler,
                     is_syslog: bool,
                     ) -> dict[str, set[tuple[str | int, ...]]]:
    """
    Iterates over logs in log file. If log contains all mandatory attributes from predicate(s) from ruleset, this
    predicate value is created. Optional attributes are filled with "_" if not contained in log.

    @param arguments: Arguments
    @param predicates_ruleset: : dict[str, list[str]]
        Predicates from the ruleset
    @param countermeasures: Countermeasures
    @param resource_handler: ResourceHandler
    @param is_syslog: bool
        Tells if parsed file is syslog or ipflow log. Syslog -> True, IPflow -> False
    @return: matched_predicates: dict[str, set[tuple[str | int, ...]]]
        Dictionary containing all created predicates
    """
    log_file_path = arguments.syslog_file_path if is_syslog else arguments.ipflow_file_path
    assert log_file_path is not None
    matched_predicates = defaultdict(set)

    for log_json in yield_json(log_file_path, arguments.start_timestamp, arguments.end_timestamp, is_syslog):
        process_log_for_static_attributes(log_json, resource_handler)
        process_log_for_countermeasures(log_json, countermeasures)

        for node, attributes in predicates_ruleset.items():
            mandatory_attributes = {p for p in attributes if not p.startswith("*")}
            if not is_subset_upper(set(log_json.keys()), set(mandatory_attributes)):
                continue
            attributes = [p[1:] if p.startswith("*") else p for p in attributes]

            matched_predicates[node].add(
                tuple([log_json[key.upper()] if key.upper() in log_json.keys() else "_" for key in attributes]))

    return matched_predicates


def add_matched_predicates_to_vertices(matched_predicates: dict[str, set[tuple[str | int, ...]]],
                                       resource_handler: ResourceHandler, ns_min: int) -> None:
    """
    Adds all the created predicate values into dict used to create the input file

    @param matched_predicates: dict[str, set[tuple[str | int, ...]]]
        Dict with predicate names as keys used to store all possible predicate values
    @param resource_handler: ResourceHandler
    @param ns_min: int
        Minimum of hostname - port pair occurrences to generate networkService attribute
    @return: None
    """
    new_network_services = filter_network_services(matched_predicates, ns_min, resource_handler.network_service_counter)
    matched_predicates["networkService"] = new_network_services

    for node, parameters_set in matched_predicates.items():
        resource_handler.vertices_dict[node] = [list(parameters) for parameters in parameters_set]


def create_input_files(arguments: Arguments, resource_handler: ResourceHandler) -> list[str]:
    """
    Creates input file for every attack goal with the generated vertices
    @param arguments: Arguments
    @param resource_handler:  ResourceHandler
    @return: input_file_paths: list[str]
        List containing paths to created input files
    """
    input_dict: dict[str, dict[str, list[str]] | dict[str, list[list[str | int]]]] = {
        "vertices": resource_handler.vertices_dict}

    assert arguments.attack_goal_file_path is not None
    attack_goals = generate_attack_goals(arguments.attack_goal_file_path)
    if len(attack_goals) == 1:
        input_dict["attackGoal"] = attack_goals[0]
        input_json: str = dumps(input_dict, indent=4)
        with open(arguments.input_file_path, "w") as file:
            file.write(input_json)
        return [arguments.input_file_path]

    input_file_paths: list[str] = []
    for i, attack_goal in enumerate(attack_goals):
        input_dict["attackGoal"] = attack_goal
        file_name, extension = os.path.splitext(arguments.input_file_path)
        cur_path = f"{file_name}_{i}{extension}"

        input_json = dumps(input_dict, indent=4)
        with open(cur_path, "w") as file:
            file.write(input_json)

        input_file_paths.append(cur_path)
        input_dict.pop("attackGoal")

    return input_file_paths


def generate_input_file(arguments: Arguments) -> list[str]:
    """
    Generates input file from given ruleset, syslog log file and ipflow flow file

    @param arguments: Arguments
    @return: input_file_paths: list[str]
        List of paths to generated input files
    """
    with open(arguments.ruleset_path) as file:
        ruleset = load(file)

    countermeasures = Countermeasures(ruleset["COUNTERMEASURE"])
    resource_handler = ResourceHandler(ruleset)

    predicates_syslog = match_predicates(arguments, ruleset["PROPERTY"], countermeasures, resource_handler, True)
    predicates_ipflow = match_predicates(arguments, ruleset["PROPERTY"], countermeasures, resource_handler, False)
    matched_predicates = merge_dicts(predicates_syslog, predicates_ipflow)

    add_matched_predicates_to_vertices(matched_predicates, resource_handler, arguments.ns_min)

    countermeasures_filled_dict = countermeasures.create_countermeasures()
    resource_handler.vertices_dict.update(countermeasures_filled_dict)

    generate_static_attributes(resource_handler)
    input_file_paths = create_input_files(arguments, resource_handler)

    return input_file_paths
