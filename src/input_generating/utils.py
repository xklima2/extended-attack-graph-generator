from itertools import product
from collections import defaultdict
from typing import Any

from src.input_generating.ip_hostname_mapper import ipfix_ip_to_hostname, get_ip_map
from src.input_generating.protocol_numbers import protocol_from_decimal
from src.input_generating.vulnerable_asset.cve_parser import Vulnerability


def app_name_in_vulnerability(app_name: str, vulnerability: Vulnerability) -> bool:
    """
    Predicate deciding if app name is mentioned in given vulnerability

    @param app_name: str
        Name of the searched app
    @param vulnerability: Vulnerability
        Vulnerability to check
    @return: app_name_in_vulnerability: bool
        True if app name is mentioned in vulerability
    """
    app_name = app_name.replace(" ", "_")
    for cpe_type in vulnerability.cpe_type:
        cpe_app_name = cpe_type.split(":")[4]
        if app_name in cpe_app_name:
            return True
    return False


def is_subset_upper(superset: set[str], subset: set[str]) -> bool:
    """
    Predicate deciding if one set of strings is case-insensitive subset of the other.

    @param superset: set[str]
        Possible superset of strings
    @param subset: set[str]
        Possible subset of strings
    @return: is_subset: bool
        True if subset is really subset of superset
    """
    return {s.upper() for s in subset}.issubset({s.upper() for s in superset})


def cartesian_product(sets: list[set[str | int]]) -> list[list[str | int]]:
    """
    Creates every ordered combination of the input sets elements

    @param sets: list[set[str | int]]
        List of sets to generate the cartesian product from
    @return: cartesian_product: list[list[str | int]]
        List containing every ordered combination of the input sets elements represented as a list
    """
    if len(sets) == 1:
        return [[x] for x in sets[0]]
    return [list(combination) for combination in product(*sets)]


def transform_ipflow(log_json: dict[str, str | int]) -> None:
    """
    Transform single ipflow flow.

    @param log_json: dict[str, str | int]
        Single ipflow flow
    @return: None
    """
    if "PROTOCOLIDENTIFIER" in log_json.keys():
        protocol = protocol_from_decimal(log_json["PROTOCOLIDENTIFIER"])
        if protocol is not None:
            log_json["PROTOCOL"] = protocol

    port = None
    if "SOURCEIPV4ADDRESS" in log_json.keys() and "DESTINATIONIPV4ADDRESS" in log_json.keys():
        if log_json["DESTINATIONIPV4ADDRESS"] not in get_ip_map():
            log_json["SOURCEIPV4ADDRESS"], log_json["DESTINATIONIPV4ADDRESS"] = (
                log_json["DESTINATIONIPV4ADDRESS"], log_json["SOURCEIPV4ADDRESS"])

            if "SOURCETRANSPORTPORT" in log_json.keys() and "DESTINATIONTRANSPORTPORT" in log_json.keys():
                log_json["SOURCETRANSPORTPORT"], log_json["DESTINATIONTRANSPORTPORT"] = (
                    log_json["DESTINATIONTRANSPORTPORT"], log_json["SOURCETRANSPORTPORT"])

        assert isinstance(log_json["DESTINATIONIPV4ADDRESS"], str)
        hostname = ipfix_ip_to_hostname(log_json["DESTINATIONIPV4ADDRESS"])
        assert hostname is not None
        log_json["HOSTNAME"] = hostname

        if log_json["SOURCEIPV4ADDRESS"] not in get_ip_map():
            log_json["SOURCEIPV4ADDRESS"] = "internet"

        if "DESTINATIONTRANSPORTPORT" in log_json.keys():
            port = log_json["DESTINATIONTRANSPORTPORT"]

        if port is not None:
            log_json["PORT"] = port


def merge_dicts(dict_1: dict[Any, set[Any]], dict_2: dict[Any, set[Any]]) -> dict[Any, set[Any]]:
    """
    Merge two dictionaries with values of type set.

    @param dict_1: dict[Any, set[Any]]
        First dict
    @param dict_2: dict[Any, set[Any]]
        Second dict
    @return: merged_dict: dict[Any, set[Any]]
        New dict representing the union of two input dicts.
    """
    merged_dict = defaultdict(set)

    for key, value in dict_1.items():
        merged_dict[key].update(value)

    for key, value in dict_2.items():
        merged_dict[key].update(value)

    return merged_dict


def filter_network_services(matched_predicates: dict[str, set[tuple[str | int, ...]]], ns_min: int,
                            ns_counter:  dict[str | int, dict[str | int, int]]) -> set[tuple[str | int, ...]]:
    """
    Helper function used to filter the networkService nodes by the minimal number of hostname - port pairs

    @param matched_predicates: dict[str, set[tuple[str | int, ...]]]
        Dict with predicate names as keys used to store all possible predicate values
    @param ns_min: int
        Minimum of hostname - port pair occurrences to generate networkService attribute
    @param ns_counter:  dict[str | int, dict[str | int, int]]
        Dictionary to count hostname port pairs occurrences
    @return: new_network_services: set[tuple[str | int, ...]]
        Filtered networkService nodes.
    """
    new_network_services = set()

    for network_service in matched_predicates["networkService"]:
        hostname, _, _, port, _ = network_service
        if ns_counter[hostname][port] >= ns_min:
            new_network_services.add(network_service)

    return new_network_services
