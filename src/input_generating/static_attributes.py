from src.input_generating.resource_handler import ResourceHandler
from src.input_generating.utils import app_name_in_vulnerability
from src.input_generating.vulnerable_asset.classifier import classifier
from src.input_generating.vulnerable_asset.cve_parser import get_dict_of_vulnerability


def generate_external_actor(resource_handler: ResourceHandler) -> None:
    """
    Statically generates external actor and adds it to the vertices dict from which is the input file generated
    Matches "externalActor": ["position"] in ruleset["LEVEL"]

    @return: None
    """
    assert resource_handler.ruleset["LEVEL"]["externalActor"] == ["position"]
    resource_handler.vertices_dict["externalActor"] = [["internet"]]


def generate_vulnerable_asset(resource_handler: ResourceHandler) -> None:
    """
    Statically generates vulnerable assets and adds those to the vertices dict
    from which the input file is generated
    Matches "vulnerableAsset": ["hostname", "app_name", "*vulID", "vector", "impact"] in ruleset["PROPERTY"]

    @return: None
    """
    assert resource_handler.ruleset["PROPERTY"]["vulnerableAsset"] == ["hostname", "app_name", "*vulID", "vector",
                                                                       "impact"]

    vulnerable_assets: set[tuple[str, str, str, str, str]] = set()
    dict_of_vulnerability = get_dict_of_vulnerability()

    impact_dict = {
        "Application availability loss": "appAvailabilityLoss",
        "Application integrity loss": "appIntegrityLoss",
        "Application confidentiality loss": "appConfidentialityLoss",
        "System availability loss": "systemAvailabilityLoss",
        "System integrity loss": "systemIntegrityLoss",
        "System confidentiality loss": "systemConfidentialityLoss",
        "Gain root/system/administrator privileges on system": "gainRootPrivileges",
        "Gain user privileges on system": "gainUserPrivileges",
        "Gain privileges on application": "gainPrivOnApp",
        "Arbitrary code execution as root/administrator/system": "gainRootPrivileges",
        "Arbitrary code execution as user of application": "gainUserPrivileges",
        "Privilege escalation on system": "privEscalation",
    }

    for hostname, app_name in resource_handler.vulnerable_asset_attributes:
        if app_name == "":
            continue
        for vul_id, vulnerability in dict_of_vulnerability.items():
            if not app_name_in_vulnerability(app_name, vulnerability) or vul_id not in vulnerability.cvssv2 == {}:
                continue
            attack_vector: str = vulnerability.cvssv3["av"]
            impacts = classifier(vulnerability)
            for imp in impacts:
                vulnerable_assets.add((hostname, app_name, vul_id.lower(), attack_vector.lower(), impact_dict[imp]))

    resource_handler.vertices_dict["vulnerableAsset"] = [list(vulnerable_asset) for vulnerable_asset in
                                                         vulnerable_assets]


def generate_has_account(resource_handler: ResourceHandler) -> None:
    """
    Statically generates hasAccount values and adds those to the vertices dict
    from which the input file is generated
    Matches "hasAccount": ["*identity", "account", "hostname", "*application"] in ruleset["PROPERTY"]

    @return: None
    """
    assert resource_handler.ruleset["PROPERTY"]["hasAccount"] == ["*identity", "account", "hostname", "*application"]

    with_root: list[list[str | int]] = [["_", "root", hostname, "_"] for hostname in resource_handler.hostnames]
    with_user: list[list[str | int]] = [["_", "user", hostname, "_"] for hostname in resource_handler.hostnames]

    if "hasAccount" not in resource_handler.vertices_dict.keys():
        resource_handler.vertices_dict["hasAccount"] = []
    resource_handler.vertices_dict["hasAccount"].extend(with_root + with_user)


def generate_static_attributes(resource_handler: ResourceHandler) -> None:
    """
    Generates everything that can be statically generated if it is contained in the ruleset

    @return: None
    """
    if "externalActor" in resource_handler.ruleset["LEVEL"].keys():
        generate_external_actor(resource_handler)

    if "vulnerableAsset" in resource_handler.ruleset["PROPERTY"].keys():
        generate_vulnerable_asset(resource_handler)

    if "hasAccount" in resource_handler.ruleset["PROPERTY"].keys():
        generate_has_account(resource_handler)
