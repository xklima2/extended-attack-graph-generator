from typing import cast

from collections import defaultdict

from src.vertices.vertex import Vertex
from src.vertices.level import Level
from src.vertices.technique import Technique

from src.enums.phases_enum import get_successor_phases, Phase


def _generate_all_paths_rec_level(all_paths_tech: list[list[Technique]], all_paths_levels: list[list[Level]],
                                  cur_level: Level, cur_path_tech: list[Technique],
                                  cur_path_level: list[Level]) -> None:
    """
    Recursive function for generating all possible paths in graph

    @param all_paths_tech: list[list[Technique]]
        list for generated paths
    @param all_paths_levels:  list[list[Level]]
        list for generated paths
    @param cur_level: Level
        Currently explored level
    @param cur_path_tech: list[Technique]
        Current explored path
    @param cur_path_level: list[Level]
        Current explored path
    @return: None
    """
    cur_path_level.append(cur_level)
    if not cur_level.previous_techniques:
        all_paths_tech.append(list(reversed(cur_path_tech)))
        all_paths_levels.append(list(reversed(cur_path_level)))

    for prev_technique in cur_level.previous_techniques:
        _generate_all_paths_rec_technique(all_paths_tech, all_paths_levels, prev_technique,
                                          cur_path_tech, cur_path_level)
    cur_path_level.pop()


def _generate_all_paths_rec_technique(all_paths_tech: list[list[Technique]], all_paths_levels: list[list[Level]],
                                      cur_tech: Technique, cur_path_tech: list[Technique],
                                      cur_path_level: list[Level]) -> None:
    """
    Recursive function for generating all possible paths in graph

    @param all_paths_tech: list[list[Technique]]
        list for generated paths
    @param all_paths_levels: list[list[Level]]
        list for generated paths
    @param cur_tech: Technique
        Currently explored technique
    @param cur_path_tech: list[Technique]
        Current explored path
    @param cur_path_level: list[Level]
        Current explored path
    @return: None
    """
    cur_path_tech.append(cur_tech)
    for condition in cur_tech.conditions:
        if isinstance(condition, Level):
            _generate_all_paths_rec_level(all_paths_tech, all_paths_levels, cast(Level, condition),
                                          cur_path_tech, cur_path_level)
    cur_path_tech.pop()


def generate_all_paths(attack_goal: Vertex) -> tuple[list[list[Technique]], list[list[Level]]]:
    """
    Generates all possible paths in graph, containing only Technique objects

    @param attack_goal: Vertex
        root of given graph
    @return all_paths_techniques: list[list[Technique]], all_paths_levels: list[list[Level]]
        List containing all possible paths represented as a list of Technique objects
        List containing all possible paths represented as a list of Level objects
    """
    all_paths_techniques: list[list[Technique]] = []
    all_paths_levels: list[list[Level]] = []

    for previous_technique in attack_goal.previous_techniques:
        _generate_all_paths_rec_technique(all_paths_techniques, all_paths_levels, previous_technique, [], [])

    return all_paths_techniques, all_paths_levels


def assign_kill_chain_phases_single_path(path_techniques: list[Technique]) -> dict[Technique, set[Phase]]:
    """
    Generates possible kill chain phases for each technique on a single path with respect to partial ordering of phases

    @param path_techniques: list[Technique]
        Path with techniques to generate kill chain phases for
    @return: dict[Technique, set[Phase]]
        Dictionary set of possible kill chain phase - value for every technique on path - key
    """
    path_phases = [technique.get_possible_kill_chain_phases() for technique in path_techniques]

    for cur_pos, cur_phases in enumerate(path_phases):
        for prev_pos in range(cur_pos):
            prev_phases = path_phases[prev_pos]
            for cur_phase in cur_phases.copy():
                for prev_phase in prev_phases.copy():
                    if prev_phase not in get_successor_phases(cur_phase):
                        continue
                    if len(prev_phases) > 1 and len(cur_phases) == 1:
                        prev_phases.remove(prev_phase)
                    else:
                        cur_phases.remove(cur_phase)

    return dict(zip(path_techniques, path_phases))


def phase_assigned_to_all_techniques(path: list[Technique]) -> bool:
    """
    Checks if every technique in path has at least one Kill chain phase assigned to it

    @param path: list[Technique]
        Path with techniques
    @return: bool
        Return True if there is at least one Kill chain phase assigned to every technique on path, False else
    """
    for technique in path:
        if len(technique.kc_phases) == 0:
            return False
    return True


def change_levels_style(all_paths_techniques: list[list[Technique]], all_paths_levels: list[list[Level]]) -> None:
    """
    Changes style to "solid" for all the levels at every path, if all techniques on that path have at least
    one Kill chain phase assigned to it

    @param all_paths_techniques: list[list[Technique]]:
        List containing all possible paths represented as a list of Technique objects
    @param all_paths_levels: list[list[Level]]
        List containing all possible paths represented as a list of Level objects
    @return: None
    """

    for path_technique, path_level in zip(all_paths_techniques, all_paths_levels):
        if not phase_assigned_to_all_techniques(path_technique):
            continue
        for level in path_level:
            level.style = "solid"
        for technique in path_technique:
            technique.style = "solid"
            for condition in technique.conditions:
                if not isinstance(condition, Level):
                    condition.style = "solid"


def assign_kill_chain_phases(attack_goal: Vertex) -> None:
    """
    Assigns possible kill chain phases to every technique in created graph

    @param attack_goal: Vertex
        root of given graph
    @return: None
    """
    phases_from_all_paths = defaultdict(lambda: [])
    all_paths_techniques, all_paths_levels = generate_all_paths(attack_goal)

    for path in all_paths_techniques:
        phases_from_cur_path = assign_kill_chain_phases_single_path(path)
        for technique, phases in phases_from_cur_path.items():
            phases_from_all_paths[technique].append(phases)

    for technique, list_phases in phases_from_all_paths.items():
        technique.kc_phases = list_phases[0].intersection(*list_phases)

    change_levels_style(all_paths_techniques, all_paths_levels)
