from src.vertices.vertex import Vertex
from src.enums.shapes_enum import Shape


class Level(Vertex):
    def __init__(self, name: str, args) -> None:
        super().__init__(name, Shape.LEVEL, args)
