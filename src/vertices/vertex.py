from typing import TYPE_CHECKING, Optional
from src.enums.colors_enum import Color

from src.graph_generating.helper import Helper
if TYPE_CHECKING:
    from src.vertices.technique import Technique


class Vertex:
    """Represents vertex of an attack graph
    """
    def __init__(self, name: str, shape: str, attributes: list[str]) -> None:
        """Creates vertex

        Parameters
        ----------
        name : str
            name of the vertex
        attributes : list[str]
            list of attributes of the vertex
        """
        self.name: str = name
        self.attributes: list[tuple[str, str]] = []
        self.next_technique: Optional[Technique] = None
        self.previous_techniques: list[Technique] = []
        self.color: Color = Color.WHITE
        self.shape = shape
        self.style = "dashed"
        for a in attributes:
            self.attributes.append((a, ""))

    def set_attributes(self, attributes: list[str]) -> None:
        """Set the values for the attributes

        Parameters
        ----------
        attributes : list[str]
            list of the attribute values
        """
        for i in range(len(attributes)):
            self.attributes[i] = (self.attributes[i][0], str(attributes[i]))

    def __eq__(self, other) -> bool:
        if not isinstance(other, Vertex):
            return NotImplemented
        return self.name == other.name and Helper.eq_both_with_variables(self, other, self.next_technique,
                                                                         other.next_technique)

    def __hash__(self) -> int:
        t: list[tuple[str, str]] = []
        for i in self.attributes:
            if Helper.is_variable(i[1]) and self.next_technique is not None:
                t.append((i[0], self.next_technique.variables[i[1]]))
            else:
                t.append(i)

        return hash((self.name, str(t)))

    def __repr__(self) -> str:
        return f"Vertex(name={self.name}, shape={self.shape}, style={self.shape}, attributes={self.attributes})"
