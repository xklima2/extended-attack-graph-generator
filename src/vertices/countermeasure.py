from src.vertices.vertex import Vertex
from src.enums.shapes_enum import Shape


class Countermeasure(Vertex):
    def __init__(self, name: str, args) -> None:
        super().__init__(name, Shape.COUNTERMEASURE, args)
