from copy import deepcopy

from typing import TYPE_CHECKING
from src.graph_generating.helper import Helper

from src.enums.shapes_enum import Shape
from src.enums.phases_enum import Phase, get_kill_chain_phases

if TYPE_CHECKING:
    from src.vertices.vertex import Vertex


class Technique:
    """Represents technique of an attack graph
    """

    def __init__(self, name: str, technique: dict, predicates: dict[str, 'Vertex']) -> None:
        """Creates the technique

        Parameters
        ----------
        name : str
            name of the technique
        technique : dict
            dict containing technique information
        predicates : dict[str, Vertex]
            dict of all the vertices which could be in the attack graph
        """
        self.key: str = name
        self.technique_name: str = technique["name"]
        self.id: str = technique["id"]
        self.conditions: list['Vertex'] = []
        # map of variables belonging to the vectors of the technique
        self.variables: dict[str, str] = {}
        self.shape = Shape.TECHNIQUE
        self.style = "dashed"
        self.kc_phases: set[Phase] = set()

        if predicates:
            self.result_vertex = deepcopy(predicates[self.key])

            self.result_vertex.set_attributes(
                technique["resultVectorAttributes"])

            # set conditions of technique -> create vectors and add them
            # we do not need to add the variables of the result_vertex,
            # since it cannot have variables which are not in the conditions
            for condition in technique["conditions"]:
                assert len(condition) == 1
                k = next(iter(condition))
                v = condition[k]

                vertex = deepcopy(predicates[k])
                vertex.set_attributes(v)
                vertex.next_technique = self
                self.set_variables(v)
                self.conditions.append(vertex)

            # for hashing
            self.result_vertex.next_technique = self

    def __eq__(self, other) -> bool:
        if not isinstance(other, Technique):
            raise RuntimeError(
                "Comparing Technique with instance of a different object")
        return (self.key == other.key and
                self.technique_name == other.technique_name and
                self.id == other.id and self.conditions == other.conditions and
                self.result_vertex == other.result_vertex)

    def __hash__(self) -> int:
        return hash((self.key, self.technique_name, self.id, self.result_vertex))

    def __repr__(self) -> str:
        return (f"Technique("
                f"name={self.key}, "
                f"technique_name={self.technique_name}, "
                f"id={self.id}, "
                f"shape={self.shape}, "
                f"style={self.style}, "
                f"kc_phases={self.kc_phases})"
                )

    def set_variables(self, lst: list[str]) -> None:
        """Sets the variables of the technique

        Parameters
        ----------
        lst : list[str]
            list of variables to be set
        """
        for potential_variable in lst:
            if len(potential_variable) > 0 and str(potential_variable)[0].isupper():
                self.variables[potential_variable] = ""

    def set_values(self, vertex: 'Vertex', inner_vertex: 'Vertex') -> None:
        """Sets the values in the variables dictionary based on outer vertex

        Parameters
        ----------
        vertex : Vertex
            from which we set the values
        inner_vertex : Vertex
            based on which we set the values
        """
        for i in range(len(inner_vertex.attributes)):
            if (Helper.is_variable(inner_vertex.attributes[i][1])
                    and not Helper.is_var_or_empty(vertex.attributes[i][1])):
                self.variables[inner_vertex.attributes[i][1]] = vertex.attributes[i][1]

    def set_values_from_variable(self, vertex: 'Vertex', inner_vertex: 'Vertex') -> None:
        """Sets the values in the variables dictionary based on outer vertex with variables

        Parameters
        ----------
        vertex : Vertex
            from which we set the values
        inner_vertex : Vertex
            based on which we set the values
        """
        for i in range(len(inner_vertex.attributes)):
            if Helper.is_variable(vertex.attributes[i][1]) and vertex.next_technique is not None:
                value = vertex.next_technique.variables[vertex.attributes[i][1]]
            else:
                value = vertex.attributes[i][1]

            if Helper.is_variable(inner_vertex.attributes[i][1]):
                self.variables[inner_vertex.attributes[i]
                               [1]] = value

    def print_kill_chain_phases(self) -> str:
        """
        Returns possible kill chain phases in text form for generating output file
        @return: kill_chain_phases: str
            Possible kill chain phases
        """
        assert self.kc_phases is not None

        if len(self.kc_phases) == 0:
            return "Unable to assign any Kill chain phase"

        if len(self.kc_phases) == 1:
            return "Kill chain phase: " + next(iter(self.kc_phases))

        return "Kill chain phases: " + ", ".join(self.kc_phases)

    def get_possible_kill_chain_phases(self) -> set[Phase]:
        """
        Returns possible kill chain phases for this Technique object
        @return: kill_chain_phases: set[Phase]
            Set of possible kill chain phases for this Technique object
        """
        return get_kill_chain_phases(self.id)
